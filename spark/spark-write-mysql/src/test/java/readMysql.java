import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SaveMode;
import org.apache.spark.sql.SparkSession;

import java.util.Properties;

public class readMysql {
    public static void main(String[] args) {
        SparkSession spark = SparkSession.builder().master("local").appName("read mysql").getOrCreate();
        readMysql(spark);
        spark.close();
    }

    public static void readMysql(SparkSession spark) {
//        String url = "jdbc:mysql://10.93.84.53:3306/big_data?characterEncoding=UTF-8";
        String url = "jdbc:mysql://test.ads-bill.ads.sg1.mysql:3306/ads_bill?characterEncoding=UTF-8";
        String table = "wlw_test_2";
        Properties props = new Properties();
        props.put("user", "jifei");
        props.put("password", "dbvqTuOkDFWBce");
        props.put("driver", "com.mysql.cj.jdbc.Driver");
        Dataset<Row> jdbc = spark.read().jdbc(url, table, props);
        System.out.println("读数据");
        jdbc.printSchema();
        table = "wlw_test_4";
        System.out.println("写数据");
        jdbc.write().mode(SaveMode.Append).jdbc(url, table, props);

    }
}
