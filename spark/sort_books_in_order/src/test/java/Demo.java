public class Demo {
    public static void main(String[] args) {
        System.out.println("abc:" + null);

        String a = "abc:" + null;
        System.out.println(System.identityHashCode(a));
        a = null + a;
        System.out.println(System.identityHashCode(a));
        System.out.println(a);


        String b = null;
        b = b + b;
        System.out.println(b);
    }
}
