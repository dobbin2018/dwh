package tools;

import java.io.File;
public class FileUtils {

    public static boolean rename(String sourceFile, String targetFile) {
        File file1 = new File(sourceFile);
        File file2 = new File(targetFile);
        return rename(file1, file2);
    }

    public static boolean rename(File sourceFile, File targetFile) {
        if (!sourceFile.exists() || targetFile.exists()) {
            return false;
        }
        if (!sourceFile.isFile()) {
            return false;
        }
        return sourceFile.renameTo(targetFile);
    }
}

