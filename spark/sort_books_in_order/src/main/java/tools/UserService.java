package tools;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

// 1. 引入slf4j接口的Logger和LoggerFactory
public class UserService {
    // 2. 声明一个Logger，这个是static的方式，我比较习惯这么写。
    private  final  static Logger logger = LogManager.getLogger();

    public  boolean verifyLoginInfo(String userName, String password) {
        // 3. log it，输出的log信息将会是："Start to verify User [Justfly]
        logger.warn("Start to verify User [{}]", userName);
        return  false;
    }

    public static void main(String[] args) {
        new UserService().verifyLoginInfo("zs","123");
    }
}
