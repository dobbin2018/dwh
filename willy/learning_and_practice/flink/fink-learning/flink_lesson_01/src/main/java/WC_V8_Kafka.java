import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.common.serialization.SimpleStringSchema;
import org.apache.flink.api.java.utils.ParameterTool;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer011;
import org.apache.flink.util.Collector;

import java.util.Properties;
import java.util.StringTokenizer;

/**
 * （1）字段过多时，建WordAndCount简化Tuple引用
 * （2）逻辑复杂时，把算子抽象成类
 * （3）使用flink参数工具ParameterTool。比如参数--hostname localhost --port 9999
 * （4）flink集群上执行: "flink run -c 主类 jar包 "
 * (5) 并行度。默认和当前机器的core有关。可以设置全局的并行度，也可以针对operator单独设置
 */
public class WC_V8_Kafka {
    public static void main(String[] args) throws Exception {
        //step1 environment of flink
        StreamExecutionEnvironment see = StreamExecutionEnvironment.getExecutionEnvironment();
        ParameterTool parameterTool = ParameterTool.fromArgs(args);
        see.setParallelism(3);//全局并行度

        //step2 input
        String topic = "wc20210601";
        Properties kafkaConsumerProperties = new Properties();
        kafkaConsumerProperties.setProperty("bootstrap.servers","192.168.152.102:9092");
        kafkaConsumerProperties.setProperty("group.id","wc20210601_consumer");
        FlinkKafkaConsumer011<String> myConsumer = new FlinkKafkaConsumer011<>(topic, new SimpleStringSchema(), kafkaConsumerProperties);
        DataStreamSource<String> inputDataStream = see.addSource(myConsumer);

        //step3 data processing. flatMap keyBy
        SingleOutputStreamOperator<WordAndCount> result = inputDataStream.flatMap(new SplitLine())
                .keyBy("word")
                .sum("count");
        // step4 output
        result.print(); //当前Operator的并行度
        // finally run or execute flink job
        see.execute("WordCount_Job");
    }

    public static class WordAndCount {
        private String word;
        private int count;

        public WordAndCount() {
        }

        public WordAndCount(String word, int count) {
            this.word = word;
            this.count = count;
        }

        @Override
        public String toString() {
            return "WordAndCount{" +
                    "word='" + word + '\'' +
                    ", count=" + count +
                    '}';
        }

        public String getWord() {
            return word;
        }

        public void setWord(String word) {
            this.word = word;
        }

        public int getCount() {
            return count;
        }

        public void setCount(int count) {
            this.count = count;
        }
    }


    public static class SplitLine implements FlatMapFunction<String, WordAndCount> {

        @Override
        public void flatMap(String line, Collector<WordAndCount> out) throws Exception {
            StringTokenizer stringTokenizer = new StringTokenizer(line);
            while (stringTokenizer.hasMoreTokens()) {
                String token = stringTokenizer.nextToken();
                out.collect(new WordAndCount(token, 1));
            }
        }
    }
}
