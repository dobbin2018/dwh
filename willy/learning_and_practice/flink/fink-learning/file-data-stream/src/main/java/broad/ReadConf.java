package broad;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

public class ReadConf {
    public static void main(String[] args) {
        String env = args[0];
        String jobName = args[1];
        //保留多长时间的状态

        Config config = ConfigFactory.load("midas_income.conf").getConfig(env).getConfig(jobName);
        int impressionParrallelism = config.getInt("impression-parallelism");
        System.out.println(impressionParrallelism);
    }
}
