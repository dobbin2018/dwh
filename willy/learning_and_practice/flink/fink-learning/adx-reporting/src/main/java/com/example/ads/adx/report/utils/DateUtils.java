package com.example.ads.adx.report.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtils {
    public static String getDayHour(long timestamp){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH");
        Date date = new Date(timestamp);
        return sdf.format(date);
    }
}
