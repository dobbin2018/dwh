package com.example.ads.adx.report.utils;

import com.example.ads.adx.report.bean.Log;

public class MyUtils {
    public final static String impressionEvent = "impression";
    public final static String requestEvent = "request";

    /**
     * 加入时间判断
     * @param event
     * @return
     */
    public static String getTableDimension(Log.Event event) {
        return event.getProduct() + event.getVCode() + event.getPid() +
                event.getNation() + event.getDsp() + event.getAdfId()+ DateUtils.getDayHour(event.getTs());

    }

    /**
     * 暂时测试用
     * @param event
     * @return
     */
    public static Log.Event resetPrice(Log.Event event) {
        long imp = event.getImp();
        return Log.Event.newBuilder().setEventName(event.getEventName())
                .setRid(event.getRid())
                .setProduct(event.getProduct())
                .setVCode(event.getVCode())
                .setPid(event.getPid())
                .setNation(event.getNation())
                .setDsp(event.getDsp())
                .setAdfId(event.getAdfId())
                .setPrice(imp > 0 ? event.getPrice() : 0)
                .setReq(event.getReq())
                .setRequestWon(event.getRequestWon())
                .setBid(event.getBid())
                .setBidWon(event.getBidWon())
                .setPartBid(event.getPartBid())
                .setImp(imp)
                .setClick(event.getClick())
                .setTs(event.getTs())
                .build();
    }

    public static Log.Event eventMerge(Log.Event event1, Log.Event event2) {
        boolean isRequest1 = requestEvent.equals(event1.getEventName());
        boolean isRequest2 = requestEvent.equals(event2.getEventName());
        String eventName = isRequest1 || isRequest2 ? requestEvent : impressionEvent;

        String rid = event1.getRid();
        String pid = event1.getPid();
        String dsp = event1.getDsp();
        String product = event1.getProduct();
        String nation = event1.getNation();
        String vCode = event1.getVCode();
        String adfId = event1.getAdfId();
        long req = event1.getReq() + event2.getReq();
        long requestWon = event1.getRequestWon() + event2.getRequestWon();
        long partBid = event1.getPartBid() + event2.getPartBid();
        long bid = event1.getBid() + event2.getBid();
        long bidWon = event1.getBidWon() + event2.getBidWon();
        long imp = event1.getImp() + event2.getImp();
        long click = event1.getClick() + event2.getClick();
        long price = event1.getPrice() + event2.getPrice();
        long ts = event1.getTs();
        if (isRequest1 && isRequest2) {//2个请求，比较最大时间戳（pid等信息以第1个为准）
            ts = Math.max(event2.getTs(), event1.getTs());//取最近的ts
        } else if (!isRequest1) {//第一个不是请求数据（pid等信息以第2个为准）
            rid = event2.getRid();
            pid = event2.getPid();
            dsp = event2.getDsp();
            product = event2.getProduct();
            nation = event2.getNation();
            vCode = event2.getVCode();
            adfId = event2.getAdfId();
            ts = event2.getTs();//ts是什么不重要
        }

        return Log.Event.newBuilder().setEventName(eventName)
                .setRid(rid)
                .setProduct(product)
                .setVCode(vCode)
                .setPid(pid)
                .setNation(nation)
                .setDsp(dsp)
                .setAdfId(adfId)
                .setPrice(price)
                .setReq(req)
                .setRequestWon(requestWon)
                .setBid(bid)
                .setBidWon(bidWon)
                .setPartBid(partBid)
                .setImp(imp)
                .setClick(click)
                .setTs(ts)
                .build();
    }


    public static String getResultJson(Log.Event event) {
        return "{\"product\":\"" + event.getProduct() + "\"," +
                "\"version_code\":\"" + event.getVCode() + "\"," +
                "\"pid\":\"" + event.getPid() + "\"," +
                "\"nation\":\"" + event.getNation() + "\"," +
                "\"dsp\":\"" + event.getDsp() + "\"," +
                "\"price\":\"" + event.getPrice() + "\"," +
                "\"adf_id\":\"" + event.getAdfId() + "\"," +
                "\"requests\":\"" + event.getReq() + "\"," +
                "\"impressions\":\"" + event.getImp() + "\"," +
                "\"clicks\":\"" + event.getClick() + "\"," +
                "\"bid\":\"" + event.getBid() + "\"," +
                "\"part_bid\":\"" + event.getPartBid() + "\"," +
                "\"bid_won\":\"" + event.getBidWon() + "\"," +
                "\"request_won\":\"" + event.getRequestWon() + "\"," +
                "\"process_time\":\"" + event.getTs() + "\"}";
    }
}
