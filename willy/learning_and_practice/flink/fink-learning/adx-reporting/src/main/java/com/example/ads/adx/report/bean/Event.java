package com.example.ads.adx.report.bean;

import org.apache.flink.api.java.tuple.Tuple17;

/**
 * 事件
 */
public class Event {

    public String eventName; //事件名,AD_AdxRequest
    public String rid; //请求id
    public String product; //包名,com.lenovo.anyshare.gps
    public String vCode;  //版本,4050369，可以转换为version_code或Unknown
    public String pid; //广告位，ad:layer_p_mpp1_v3
    public String nation; //国家
    public String dsp; //dsp名称（请求明细）,shareitcpi
    public String price="0"; //出价（从曝光日志计算），
    public String adfId; //素材规格（请求明细）。在params中
    public String req = "0";    //请求1次记为1，否则0
    public String imp = "0";    //曝光1次为记1，否则0
    public String click = "0";  //点击1次为记1，否则0
    public String bid = "0";  //竞价1次记为1。条件：sts=1，即广告可以正常返回
    public String partBid = "0";  //参数竞价1次为1。条件：sts!=2且sts!=8。注意素材不合格时，也算参与竞
    public String bidWon = "0"; //竞价或请求胜出1次，记为1。条件：sts=1，且dsp胜出
    public String requestWon = "0"; //等价于bidWon
    public String timestamp = "1000010000100"; //事件时间戳

    public String getTableDimension() {
        return product + vCode + pid + nation + dsp + adfId;
    }
    public String getDistinctKey() {
        return eventName+rid+product + vCode + pid + nation + dsp + adfId;
    }
    public Event add(Event event){
        price = ""+(Float.parseFloat(price)+ Float.parseFloat(event.price));
        req = ""+(Long.parseLong(req)+ Long.parseLong(event.req));
        imp = ""+(Long.parseLong(imp)+ Long.parseLong(event.imp));
        click = ""+(Long.parseLong(click)+ Long.parseLong(event.click));
        bid = ""+(Long.parseLong(bid)+ Long.parseLong(event.bid));
        partBid = ""+(Long.parseLong(partBid)+ Long.parseLong(event.partBid));
        bidWon = ""+(Long.parseLong(bidWon)+ Long.parseLong(event.bidWon));
        requestWon = ""+(Long.parseLong(requestWon)+ Long.parseLong(event.requestWon));
        timestamp = event.timestamp.compareTo(timestamp) > 0 ? event.timestamp : timestamp;
        return this;
    }
    public Tuple17<String, String, String, String, String,
            String, String, String, String, String,
            String, String, String, String, String,
            String, String> getTuple() {
        return new Tuple17<>(eventName, rid, product, vCode, pid, nation, dsp, price, adfId, req, imp, click, bid, partBid, bidWon, requestWon, timestamp);
    }

    public Event readByLine(String line) {
//        System.out.println(line);
        String[] cols = line.split("\t");
        int index = 0;
        eventName = cols[index++];
        rid = cols[index++];
        product = cols[index++];
        vCode = cols[index++];
        pid = cols[index++];
        nation = cols[index++];
        dsp = cols[index++];
        price = cols[index++];
        adfId = cols[index++];
        req = cols[index++];
        imp = cols[index++];
        click = cols[index++];
        bid = cols[index++];
        partBid = cols[index++];
        bidWon = cols[index++];
        requestWon = cols[index++];
        timestamp = cols[index++];
        return this;
    }

    @Override
    public String toString() {
        return eventName + "\t" +
                rid + "\t" +
                product + "\t" +
                vCode + "\t" +
                pid + "\t" +
                nation + "\t" +
                dsp + "\t" +
                price + "\t" +
                adfId + "\t" +
                req + "\t" +
                imp + "\t" +
                click + "\t" +
                bid + "\t" +
                partBid + "\t" +
                bidWon + "\t" +
                requestWon + "\t" +
                timestamp;
    }
    public String toJsonString() {
        return "{\"product\":\"" + product+"\"," +
                "\"version_code\":\"" + vCode+"\"," +
                "\"pid\":\"" + pid+"\"," +
                "\"nation\":\"" + nation+"\"," +
                "\"dsp\":\"" + dsp+"\"," +
                "\"price\":\"" + price+"\"," +
                "\"adf_id\":\"" + adfId+"\"," +
                "\"requests\":\"" + req+"\"," +
                "\"impressions\":\"" + imp+"\"," +
                "\"clicks\":\"" + click+"\"," +
                "\"bid\":\"" + bid+"\"," +
                "\"part_bid\":\"" + partBid+"\"," +
                "\"bid_won\":\"" + bidWon+"\"," +
                "\"request_won\":\"" + requestWon+"\"," +
                "\"process_time\":\"" + timestamp+"\"}";
    }
}