package com.example.ads.adx.report.utils;

public class Constant {
    public final static String impressionEvent = "impression";
    public final static String requestEvent = "request";
    public final static String blankString = "";
}
