package study.flink;

import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.streaming.api.functions.AssignerWithPeriodicWatermarks;
import org.apache.flink.streaming.api.watermark.Watermark;
import study.flink.bean.Event;

import javax.annotation.Nullable;

public class BoundedOutOfOrdernessGenerator implements AssignerWithPeriodicWatermarks<Tuple2<String, String>> {


    private final long maxOutOfOrderness = 10000;

    private long currentMaxTimestamp;

    @Nullable
    @Override
    public Watermark getCurrentWatermark() {
        long timestamp = currentMaxTimestamp - maxOutOfOrderness;
        return new Watermark(timestamp);
    }

    @Override
    public long extractTimestamp(Tuple2<String, String> element, long previousElementTimestamp) {
        long timestamp = Long.parseLong(new Event().readByLine(element.f1).timestamp);
        currentMaxTimestamp =  Math.max(timestamp, currentMaxTimestamp);
        return timestamp;

    }
}
