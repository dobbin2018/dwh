package study.flink.bean;

public enum EventEnum {
    //        AD_AdxRequest
    Request("AD_AdxRequest", 1),
    Show("AD_AdxShowed", 2),
    Click("AD_AdxClicked", 3);
    private String name;
    private int id;

    EventEnum(String name, int id) {
        this.name = name;
        this.id = id;
    }

    public String getName() {
        return name;
    }


    public int getId() {
        return id;
    }

}