package study.flink;

import org.apache.flink.api.common.serialization.SimpleStringSchema;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaProducer;

import java.util.Properties;

public class SinkDemo {
    private static final String READ_TOPIC = "student";
    public static void main(String[] args) throws Exception {
        final StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        Properties consumerKafkaProps = new Properties();
        consumerKafkaProps.put("bootstrap.servers", "localhost:9092");
//        props.put("zookeeper.connect", "localhost:2181");
        consumerKafkaProps.put("group.id", "student-group");
        consumerKafkaProps.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        consumerKafkaProps.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        consumerKafkaProps.put("auto.offset.reset", "latest");

        DataStreamSource<String> student = env.addSource(new FlinkKafkaConsumer<>(
                READ_TOPIC,   //这个 kafka topic 需要和上面的工具类的 topic 一致
                new SimpleStringSchema(),
                consumerKafkaProps)).setParallelism(1);
        student.print();

        Properties producerKafkaProps = new Properties();
        producerKafkaProps.setProperty("bootstrap.servers", "localhost:9092");
        producerKafkaProps.setProperty("group.id", "student-write");

        student.addSink(new FlinkKafkaProducer<String>(
                "localhost:9092",
                "student-write",
                new SimpleStringSchema()
        )).name("flink-connectors-kafka")
                .setParallelism(5);

        env.execute("flink learning connectors kafka");
    }
}
