import junit.framework.TestCase;
import org.junit.Assert;
import org.junit.Test;

public class MyStatelessMapTest extends TestCase {

    @Test
    public void testMap() throws Exception {
        MyStatelessMap statelessMap = new MyStatelessMap();
        String out = statelessMap.map("world");
        System.out.println(out);
        Assert.assertEquals("hello world1", out);
    }
}