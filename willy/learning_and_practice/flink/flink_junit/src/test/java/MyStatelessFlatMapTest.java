import junit.framework.TestCase;
import org.apache.flink.api.common.functions.util.ListCollector;
import org.apache.flink.shaded.curator.org.apache.curator.shaded.com.google.common.collect.Lists;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class MyStatelessFlatMapTest extends TestCase {

    @Test
    public void testFlatMap() throws Exception {
        MyStatelessFlatMap statelessFlatMap = new MyStatelessFlatMap();
        List<String> out = new ArrayList<>();
        ListCollector<String> listCollector = new ListCollector<>(out);
        statelessFlatMap.flatMap("world", listCollector);
        Assert.assertEquals(Lists.newArrayList("hello world"), out);
    }

}