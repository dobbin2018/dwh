import junit.framework.TestCase;
import org.apache.flink.api.common.state.ValueState;
import org.apache.flink.api.common.state.ValueStateDescriptor;
import org.apache.flink.api.common.typeinfo.Types;
import org.apache.flink.shaded.curator.org.apache.curator.shaded.com.google.common.collect.Lists;
import org.apache.flink.streaming.api.operators.StreamFlatMap;
import org.apache.flink.streaming.runtime.streamrecord.StreamRecord;
import org.apache.flink.streaming.util.KeyedOneInputStreamOperatorTestHarness;
import org.apache.flink.streaming.util.OneInputStreamOperatorTestHarness;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class StatefulFlatMapTest {

    @Test
    public void testFlatMap() throws Exception{
        StatefulFlatMap statefulFlatMap = new StatefulFlatMap();

        //模拟上下文生成
        OneInputStreamOperatorTestHarness<String, String> testHarness =
                new KeyedOneInputStreamOperatorTestHarness<>(
                        new StreamFlatMap<>(statefulFlatMap), x -> "1", Types.STRING);
        testHarness.open();

        // test first record
        testHarness.processElement("world", 10);
        ValueState<String> previousInput =
                statefulFlatMap.getRuntimeContext().getState(
                        new ValueStateDescriptor<>("previousInput", Types.STRING));
        String stateValue = previousInput.value();
        Assert.assertEquals(
                Lists.newArrayList(new StreamRecord<>("hello world", 10)),
                testHarness.extractOutputStreamRecords());
        Assert.assertEquals("world", stateValue);

        // test second record
        testHarness.processElement("parallel", 20);
        Assert.assertEquals(
                Lists.newArrayList(
                        new StreamRecord<>("hello world", 10),
                        new StreamRecord<>("hello parallel world", 20)),
                testHarness.extractOutputStreamRecords());
        Assert.assertEquals("parallel", previousInput.value());
    }
}