import org.apache.flink.api.common.functions.MapFunction;

public class MyStatelessMap implements MapFunction<String, String> {
    @Override
    public String map(String in) throws Exception {
        String out = "hello " + in;
        return out;
    }
}