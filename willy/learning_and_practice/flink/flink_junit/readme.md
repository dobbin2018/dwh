# flink代码单元测试
##### 测试内容
单元测试 flink函数如下：
1. MapFunction
2. FlatMapFunction
3. RichFlatMapFunction

问题：

（1）相关class的依赖包
测试FlatMapFunction使用的List类 

org.apache.flink.shaded.curator.org.apache.curator.shaded.com.google.common.collect.Lists
```xml
<dependency>
    <groupId>org.apache.flink</groupId>
    <artifactId>flink-test-utils_2.11</artifactId>
    <version>1.8.0</version>
    <scope>test</scope>
</dependency>
```
测试RichFlatMapFunction使用的OneInputStreamOperatorTestHarness类

import org.apache.flink.streaming.util.OneInputStreamOperatorTestHarness;


相关博客：

https://blog.csdn.net/qq_40771567/article/details/116156944