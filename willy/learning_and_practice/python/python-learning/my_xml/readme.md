##### 根元素和子元素
```xml
<?xml version="1.0" encoding="UTF-8"?>
<note>
    <to>Tove</to>
    <from>Jani</from>
    <heading>Reminder</heading>
    <body>Don't forget me this weekend!</body>
</note>
```
根元素：ｎｏｔｅ
子元素：ｔｏ，ｆｒｏｍ，ｈｅａｄｉｎｇ，ｂｏｄｙ
