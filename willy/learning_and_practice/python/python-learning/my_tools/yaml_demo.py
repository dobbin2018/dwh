# -*- coding: utf-8 -*-

def test_file(afile):
    print(afile)
    print('*' * 8)
    with open(afile, mode='r') as fr:
        for line in fr.readlines():
            print(line)


if __name__ == '__main__':
    with open(r'D:\develop\code\002\node-data\ADS\aaa.txt', mode='r') as fr:
        for line in fr.readlines():
            print(line)