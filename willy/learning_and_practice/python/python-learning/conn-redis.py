"""
项目python环境，选择python3.8
    conda create --name py27 python=2.7
    conda create --name py38 python=3.8
pip install redis
"""

import redis

print(redis.VERSION)
# r = redis.StrictRedis(host='localhost', port=6379, db=0)
r = redis.StrictRedis(host='prod.app-category.ads.sg1.redis', port=6379)
r.set('foo', 'bar')
r.get('foo')
