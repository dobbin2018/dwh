import time

"""
这里的deco函数就是最原始的装饰器，它的参数是一个函数，然后返回值也是一个函数。

带有固定参数的装饰器
"""


def deco(f):
    def wrapper(a, b):
        start_time = time.time()
        f(a, b)
        end_time = time.time()
        execution_time = (end_time - start_time) * 1000
        print("time is %d ms" % execution_time)

    return wrapper


@deco
def f(a, b):
    print("be on")
    time.sleep(1)
    print("result is %d" % (a + b))


if __name__ == '__main__':
    f(3, 4)
