import time

"""
这里的deco函数就是最原始的装饰器，它的参数是一个函数，然后返回值也是一个函数。

无固定参数的装饰器
"""


def deco(f):
    def wrapper(*args, **kwargs):
        start_time = time.time()
        f(*args, **kwargs)
        end_time = time.time()
        execution_time = (end_time - start_time) * 1000
        print("time is %d ms" % execution_time)

    return wrapper


@deco
def f(a, b):
    print("be on")
    time.sleep(1)
    print("result is %d" % (a + b))


@deco
def f2(a, b, c):
    print("be on")
    time.sleep(1)
    print("result is %d" % (a + b + c))


if __name__ == '__main__':
    f2(3, 4, 5)
    f(3, 4)
