import time
"""
这里的deco函数就是最原始的装饰器，它的参数是一个函数，然后返回值也是一个函数。
"""

def deco(f):
    def wrapper():
        start_time = time.time()
        f()
        end_time = time.time()
        execution_time = (end_time - start_time) * 1000
        print("time is %d ms" % execution_time)

    return wrapper


@deco
def f():
    print("hello")
    time.sleep(1)
    print("world")


if __name__ == '__main__':
    f()
