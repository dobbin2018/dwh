# Flask学习
##### 参考文章
[如何通过flask运行或自定义命令运行Flask应用程序而不手动设置FLASK_APP](https://cloud.tencent.com/developer/ask/209526)

### 环境准备
导出项目python依赖到requirements.txt
    pip freeze >requirements.txt
安装requirements.txt文件中依赖
    pip install -r requirements.txt
查看已经安装的模块
    pip list
