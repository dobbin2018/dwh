import requests

auth_url = "https://platform.ironsrc.com/partners/publisher/auth"
stat_date = '2021-09-14'
payload = {}
headers = {
    'secretkey': 'd4f8b2dbea1cef92def8db28665adca5',
    'refreshToken': '3954cc0bdc71534172c89d26cf8d2f58'
}

response = requests.request("GET", auth_url, headers=headers)

bearer_str = response.text
print('=' * 8, 'Authorization is %s' % bearer_str)

url = "https://platform.ironsrc.com/partners/publisher/mediation/applications/v6/stats?" \
      "startDate={stat_date}&endDate={stat_date}" \
      "&appKey=10a80a57d" \
      "&metrics=revenue,appRequests,appFills,impressions,clicks" \
      "&breakdowns=country,instance".format(stat_date=stat_date)
print('=' * 8, 'url is %s' % url)
headers = {
    'Authorization': 'Bearer %s' % bearer_str.replace('"', ''),
    "Content-Type": "application/json",
    "Accept": "application/json"
}

response2 = requests.request("GET", url, headers=headers, data=payload)

print(response2.text)
