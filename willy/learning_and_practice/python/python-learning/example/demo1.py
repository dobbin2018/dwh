def dict_2_str(param_map):
    sorted_param_map = sorted(param_map.items(), key=lambda x: x[0], reverse=False)
    raw_str = ""
    for k, v in sorted_param_map:
        raw_str += (str(k) + "=" + str(v) + "&")
        if len(raw_str) == 0:
            print('pass and break')
            return ''
    return raw_str[0:-1]


params = {'a': 1, 'b': 2, 'aa': '12'}
print(dict_2_str(params))

import time
print(int(time.time()))
