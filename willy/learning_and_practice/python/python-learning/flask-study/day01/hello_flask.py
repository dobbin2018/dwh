# hello.py
"""
# window
set FLASK_APP=hello.py
# linux
export FLASK_APP=hello.py
如果你没有指定FLASK_APP环境变量，flask-study 运行的时候首先会尝试自动去项目的根目录下的 wsgi.py 文件 或者 app.py 文件里面去找。
没找到就会报 NoAppException 错误。
"""
from flask import Flask

# (1) 模块名
app = Flask(__name__)


# (2)定义视图
@app.route('/')
def foo():
    return 'hello, flask.'


# （3）启动
if __name__ == '__main__':
    app.run()
