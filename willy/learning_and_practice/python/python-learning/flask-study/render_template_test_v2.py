"""
模板template使用
使用redder_template的情况
"""
from flask import Flask, render_template

app = Flask(__name__)


@app.route('/')
@app.route('/index')
def index():
    user = {'nickname': 'Miguel'}  # fake user
    return render_template("index.html",
                           title='Home',
                           user=user)  # 这里模块里的第一个user指的是html里面的变量user，而第二个user指的是函数index里面的变量user


if __name__ == '__main__':
    app.run()
