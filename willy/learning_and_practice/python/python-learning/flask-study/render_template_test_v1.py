"""
不适用redder_template的情况
"""
from flask import Flask

app = Flask(__name__)


@app.route('/')
@app.route('/index')
def index():
    user = {'nickname': 'Miguel'}  # fake user
    return '''
    <html>
        <head>
            <title>Home Page</title>
        </head>
        <body>
            <h1>Hello, ''' + user['nickname'] + '''</h1>
        </body>
    </html>
    '''


if __name__ == '__main__':
    app.run()
