"""
静态文件访问

(1)默认地通过http://localhost:5000/static/<file.name>访问
(2)通过参数static_url_path使通过http://localhost:5000/s/<file.name>也可以访问
(3)Flask初始化参数
        import_name, 模块名称
        static_url_path=None, 自定义静态文件的访问路径
        static_folder="static", 静态文件目录
        static_host=None,
        host_matching=False,
        subdomain_matching=False,
        template_folder="templates", 模板目录
        instance_path=None,
        instance_relative_config=False,
        root_path=None,

"""

from flask import Flask

app = Flask(__name__, static_url_path='/s')


# (2)定义视图
@app.route('/')
def foo():
    return 'hello, flask.'


# （3）启动
if __name__ == '__main__':
    app.run()
