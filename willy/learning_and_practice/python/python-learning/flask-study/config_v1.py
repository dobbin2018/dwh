"""
加载配置


通过自定义对象类，加载配置信息
"""

from flask import Flask


# (1) 定义对象的类
class DefaultConfig(object):
    SECRET_KEY = '0cc48233f8970c695a3ee0d88e81d48f'


app = Flask(__name__)
# （2）加载配置
app.config.from_object(DefaultConfig)


@app.route('/')
def foo():
    print(app.config.get('SECRET_KEY'))
    return 'hello, flask.<%s>' % app.config.get('SECRET_KEY')


if __name__ == '__main__':
    app.run()
