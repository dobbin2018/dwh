"""
加载配置


通过py文件方式，加载配置信息
"""

from flask import Flask

app = Flask(__name__)
# 加载配置
app.config.from_pyfile('config.py')


@app.route('/')
def foo():
    print(app.config.get('SECRET_KEY'))
    return 'hello, flask.<%s>' % app.config.get('SECRET_KEY')


if __name__ == '__main__':
    app.run()
