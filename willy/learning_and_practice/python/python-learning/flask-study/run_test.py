"""
新的运行方式：
(1) 不需要指定app.run
(2) 依赖FLASK_APP环境变量
(3) 执行flask run
好处1：run参数启动时指定，如--port 5000
默认FLASK_ENV是production.其他development


示例1：
cd <my flask dir>
export FLASK_APP=run_test
flask run [--port 8000]
"""
from flask import Flask

# (1) 模块名
app = Flask(__name__)


# (2)定义视图
@app.route('/')
def foo():
    return 'hello, flask.'


# （3）启动
if __name__ == '__main__':
    app.run(host=None, port=None, debug=None, load_dotenv=True)
