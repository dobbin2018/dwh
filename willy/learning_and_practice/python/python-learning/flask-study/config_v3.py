"""
加载配置


通过环境变量，加载配置信息
export MY_CONFIG=config.py
"""

from flask import Flask

app = Flask(__name__)
# 加载配置
app.config.from_envvar('MY_CONFIG', silent=True)


@app.route('/')
def foo():
    print(app.config.get('SECRET_KEY'))
    return 'hello, flask.<%s>' % app.config.get('SECRET_KEY')


if __name__ == '__main__':
    app.run()
