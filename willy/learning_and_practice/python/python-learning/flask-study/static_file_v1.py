"""
静态文件访问

(1)默认地通过http://localhost:5000/static/<file.name>访问
(2)通过参数static_url_path使通过http://localhost:5000/s/<file.name>也可以访问


"""

from flask import Flask

app = Flask(__name__, static_url_path='/s')


# (2)定义视图
@app.route('/')
def foo():
    return 'hello, flask.'


# （3）启动
if __name__ == '__main__':
    app.run()
