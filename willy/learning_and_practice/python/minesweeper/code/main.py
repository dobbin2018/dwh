import sys

import pygame
from pygame.locals import *

pygame.init()


while True:
    for event in pygame.event.get():
        pygame.quit()
        sys.exit()
    pygame.display.update()