package my.typesafe;
import com.typesafe.config.ConfigFactory;
import com.typesafe.config.Config;
public class Reader {
    public static void main(String[] args) {
        Config config = ConfigFactory.load("my_typesafe.conf").getConfig("prod");
        System.out.println(config.getConfig("flink").getString("default-parallelism"));
        System.out.println(config.getString("flink.default-parallelism"));
        System.out.println(config.getConfig("source"));
    }
}
