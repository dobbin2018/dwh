package com.example.http_data_pull;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HttpDataPullApplication {

	public static void main(String[] args) {
		SpringApplication.run(HttpDataPullApplication.class, args);
	}

}
