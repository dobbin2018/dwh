import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * 利用java.lang.Runtime运行主机上的wget命令
 */
public class InvokeWGet {
    public static void main(String[] args) throws IOException {
        String cmdStr = "wget https://baidu.com";
        System.out.println(cmdStr);
        Runtime runtime = Runtime.getRuntime();
        Process exec = runtime.exec(cmdStr);
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(exec.getErrorStream()));
        System.out.println("------");
        String s;
        while ((s=bufferedReader.readLine())!=null){
            System.out.println(s);
        }
        bufferedReader.close();
    }
}
