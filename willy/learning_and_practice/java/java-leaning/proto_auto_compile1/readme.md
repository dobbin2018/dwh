# ProtoBuffer自动编译--maven和本地安装的protoc联合
特别注意，maven的protobuf-java和本地protoc的版本不一致时，可能报错。比如：
> Error:(403,18) java: 对于parseFrom(java.nio.ByteBuffer), 找不到合适的方法
> Error:(818,7) java: 对于internalBuildGeneratedFileFrom(java.lang.String[],com.google.protobuf.Descriptors.FileDescriptor[]), 找不到合适的方法


通过maven-antrun-plugin插件。(http://maven.apache.org/plugins/maven-antrun-plugin/)

核心pom.xml配置

maven依赖
```xml
<dependencies>
    <dependency>
        <groupId>com.google.protobuf</groupId>
        <artifactId>protobuf-java</artifactId>
        <version>3.12.2</version>
    </dependency>

    <dependency>
        <groupId>commons-codec</groupId>
        <artifactId>commons-codec</artifactId>
        <version>1.4</version>
        <scope>provided</scope>
    </dependency>
</dependencies>
```
build配置
```xml
<plugin>
    <artifactId>maven-antrun-plugin</artifactId>
    <executions>
        <execution>
            <id>generate-sources1</id>
            <phase>process-test-resources</phase>
            <configuration>
                <tasks>
                    <exec executable="protoc">
                        <arg value="--java_out=src/main/java"/>
                        <arg value="proto/person.proto"/>
                    </exec>
                </tasks>
            </configuration>
            <goals>
                <goal>run</goal>
            </goals>
        </execution>
    </executions>
</plugin>
```
#### 缺点
每增加一下proto文件，都要在pom.xml中指定。不利于维护。

现命令行里,如下命令是可以运行的。但在`tasks`设置中，会提示*.proto文件不存在。
```bash
protoc \
--proto_path=/Users/msxr/develop/code/003/dwh/java-leaning/proto \
--java_out=src/main/java \
/Users/msxr/develop/code/003/dwh/java-leaning/proto/*.proto 
```
maven配置文件中的task，不完全等于shell命令行的直接调用。

为解决*.proto文件识别，现在有两条思路：
1. 遍历*代表所有文件，然把文件拼接起来
2. 本身就支持通配符