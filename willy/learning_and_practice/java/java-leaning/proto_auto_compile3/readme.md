# ProtoBuffer自动编译--maven单独完成
maven官方提供的ant插件`maven-antrun-plugin`对比，插件`protobuf-maven-plugin`的优点是，配置简单，更易用，对初学者更友好。
```xml
<project>
    <properties>
        <protobuf-java.version>3.0.0</protobuf-java.version>
        <os-maven-plugin-version>1.6.2</os-maven-plugin-version>
        <protobuf-maven-plugin-version>0.6.1</protobuf-maven-plugin-version>
    </properties>
    <dependencies>
        <!--proto buffer maven依赖-->
        <dependency>
            <groupId>com.google.protobuf</groupId>
            <artifactId>protobuf-java</artifactId>
            <version>${protobuf-java.version}</version>
        </dependency>
    </dependencies>
    <build>
        <extensions>
            <!--获取系统信息的扩展-->
            <extension>
                <groupId>kr.motd.maven</groupId>
                <artifactId>os-maven-plugin</artifactId>
                <version>${os-maven-plugin-version}</version>
            </extension>
        </extensions>
        <plugins>
            <plugin>
                <!--protobuf编译插件protobuf-maven-plugin-->
                <groupId>org.xolstice.maven.plugins</groupId>
                <artifactId>protobuf-maven-plugin</artifactId>
                <version>${protobuf-maven-plugin-version}</version>
                <configuration>
                    <protocArtifact>
                        com.google.protobuf:protoc:${protobuf-java.version}:exe:${os.detected.classifier}
                    </protocArtifact>
                    <!--默认配置-->
                    <attachDescriptorSet>false</attachDescriptorSet>
                    <attachProtoSources>true</attachProtoSources>
                    <descriptorSetFileName>${project.build.finalName}.protobin</descriptorSetFileName>
                    <descriptorSetOutputDirectory>${project.build.directory}/generated-resources/protobuf/descriptor-sets</descriptorSetOutputDirectory>
                    <hashDependentPaths>true</hashDependentPaths>
                    <outputDirectory>${project.build.directory}/generated-sources/protobuf/java</outputDirectory>
                    <protoSourceRoot>${basedir}/src/main/proto</protoSourceRoot>
                    <temporaryProtoFileDirectory>${project.build.directory}/protoc-dependencies</temporaryProtoFileDirectory>
                    <writeDescriptorSet>false</writeDescriptorSet>
                    <outputDirectory>${basedir}/src/main/java</outputDirectory>
                </configuration>
                <executions>
                    <execution>
                        <id>compile</id>
                        <goals>
                            <goal>compile</goal>
                        </goals>
                    </execution>
                    <execution>
                        <id>test-compile</id>
                        <goals>
                            <goal>test-compile</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
        </plugins>
    </build>
</project>
```


##### 源代码目录
java源代码，在target/generated-sources下。
通过修改插件`protobuf-maven-plugin`配置，把代码生成在主代码中。如下
```xml
<configuration>
    <outputDirectory>${basedir}/src/main/java</outputDirectory>
</configuration>
```
需要注意的是，outputDirectory下的所有文件在编译前会被清空

##### proto文件目录
默认的proto文件目录，是当前项目的src/main/proto的：
```xml
<protoSourceRoot>${basedir}/src/main/proto</protoSourceRoot>
```
对笔者而言，可能需要以下修改

1. proto文件在当前项目的父目录
    > ${project.parent.basedir}/proto
2. proto文件在当前项目的src的平级目录
    > ${basedir}/proto
3. proto文件在系统任一目录
    > /Users/tom/any/dir/proto

参考：https://w3sun.com/1287.html

参数配置：https://www.xolstice.org/protobuf-maven-plugin/compile-mojo.html