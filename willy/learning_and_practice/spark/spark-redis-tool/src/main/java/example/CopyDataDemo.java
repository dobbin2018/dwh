package example;

import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import scala.Tuple2;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

public class CopyDataDemo {
    private static final Pattern SPACE = Pattern.compile(" ");

    public static void main(String[] args) {
        //1.新建一个spark session
        SparkSession spark = SparkSession
                .builder()
                .appName("JavaWordCount")
                .getOrCreate();

        //2.创建上下文
//        JavaSparkContext jsc = new JavaSparkContext(spark.sparkContext());
        //3.读数据
//        Dataset<Row> parquet = spark.read().parquet("D:\\develop\\tmp\\0000_part_00.parquet");
//        JavaRDD<Row> rowJavaRDD = parquet.javaRDD();
        JavaRDD<String> lines = spark.read().textFile("D:\\develop\\tmp\\part-py-1631281108-6587.txt").javaRDD();
        JavaRDD<String> words = lines.flatMap(s -> Arrays.asList(SPACE.split(s)).iterator());

        JavaPairRDD<String, Integer> ones = words.mapToPair(s -> new Tuple2<>(s, 1));

        JavaPairRDD<String, Integer> counts = ones.reduceByKey(Integer::sum);

        List<Tuple2<String, Integer>> output = counts.collect();
        for (Tuple2<?, ?> tuple : output) {
            System.out.println(tuple._1() + ": " + tuple._2());
        }
        spark.stop();

    }
}
