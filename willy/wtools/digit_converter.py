# 进制转换
# 参考：https://www.cnblogs.com/dylancao/p/10039062.html
# 一、整数之间的进制转换:
# hex(16)     # 10进制转16进制
# oct(8)      # 10进制转8进制
# bin(8)      # 10进制转2进制
# 二、字符串转整数
# int('10')       # 字符串转换成10进制整数
# int('10',16)    # 字符串转换成16进制整数
# int('0x10',16)  # 字符串转换成16进制整数
# int('10',8)     # 字符串转换成8进制整数
# int('010',8)    # 字符串转换成8进制整数
# int('10',2)     # 字符串转换成2进制整数


def rgb(r, g, b):
    print('===input num is:\n ({}, {}, {})'.format(r, g, b))
    print('===RGB code is:')
    print(hex(r)[2:] + hex(g)[2:] + hex(b)[2:])


if __name__ == '__main__':
    rgb(127, 166, 173)
    print(int('10', 16))
    print(oct(8))
    print(bin(8))
    print(int('0x10', 16))
