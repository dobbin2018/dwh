import requests
from bs4 import BeautifulSoup

url = 'http://finance.sina.com.cn/realstock/company/sh600519/nc.shtml'

headers = {
    'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.3'}

response = requests.get(url, headers=headers)

html = response.text

soup = BeautifulSoup(html, 'html.parser')

# 获取股票代码和名称
stock_name = soup.find('h1', class_='name').text.split()[0]
stock_code = soup.find('a', class_='code').text.split()[0]

# 获取股票实时数据
data = soup.find('div', class_='act_info').find_all('span')

# 输出实时数据
print(f'{stock_name}（{stock_code}）的实时数据：')
for item in data:
    print(f'{item.text} ', end=' ')