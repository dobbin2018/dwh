package org.example.file;

import java.io.*;

public class ReadFileByUTF8 {
    public static void main(String[] args) throws IOException {
        String path="/Users/dobbin/develop/tmp/a";
        FileReader fileReader = new FileReader(path);
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        String line = bufferedReader.readLine();
        System.out.println(line);
        line = bufferedReader.readLine();
        System.out.println(line);
        line = bufferedReader.readLine();
        System.out.println(line);
    }
}
