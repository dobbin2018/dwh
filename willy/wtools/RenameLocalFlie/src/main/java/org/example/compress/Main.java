package org.example.compress;

import java.io.File;

public class Main {

    public static void main(String[] args) throws Exception {
        String password = "msxr369";
//        String inPath = "D:\\develop\\tmp\\test";
//        String commandPath = "D:\\develop\\bin\\7zr.exe";
//        windows(inPath, commandPath,password);
        String inPath = "/Users/dobbin/.ww/asmr";
        String commandPath = "/Users/dobbin/develop/etc/7z2300-mac/7zz";
        mac(inPath, commandPath,password);


    }

    public static void mac(String inPath, String commandPath, String password) throws Exception {


        FileTools.removeWhitespace(new File(inPath), "recurse");

        String zipFileName = "abc.7z";
        String mode = "recurse";
        String workspace = inPath + "_zip";
        new File(workspace).mkdir();
        CompressBy7z.fileToZip(inPath, zipFileName, commandPath, mode, workspace, password);
    }

    public static void windows(String inPath, String commandPath, String password) throws Exception {

        String zipFileName = "abc.7z";
        String mode = "recurse";
        FileTools.removeWhitespace(new File(inPath), "recurse");
        String workspace = inPath + "_zip";
        new File(workspace).mkdir();
        CompressBy7z.fileToZip(inPath, zipFileName, commandPath, mode, workspace, password);
    }

}
