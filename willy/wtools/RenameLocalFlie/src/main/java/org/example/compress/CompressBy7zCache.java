package org.example.compress;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * 解压文件
 */
public class CompressBy7zCache extends Thread {
    InputStream is;
    String type;

    public CompressBy7zCache(InputStream is, String type) {
        this.is = is;
        this.type = type;
    }

    public void run() {
        try {
//            InputStreamReader isr = new InputStreamReader(is, Charset.forName(""));
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader br = new BufferedReader(isr);
            String line;
            while ((line = br.readLine()) != null) {
                System.out.println(type + ">" + line);
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

}
