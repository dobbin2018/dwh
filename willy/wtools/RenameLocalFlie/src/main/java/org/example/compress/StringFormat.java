package org.example.compress;

import java.text.MessageFormat;
import java.util.regex.Pattern;

public class StringFormat {
    public static String format(String fmt, Object... values) {
        int i = 0;
        while(fmt.contains("{}")) {
            fmt = fmt.replaceFirst(Pattern.quote("{}"), "{"+ i++ +"}");
        }
        return MessageFormat.format(fmt, values);
    }

    public static void main(String[] args) {
        System.out.println(format("{}+{}={}",1,3,1+3));;
    }
}
