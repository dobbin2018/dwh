package org.example.compress;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Stream;

public class FileTools {
    private static FileTools fileTools;

    public static FileTools getInstance() {
        return fileTools == null ? new FileTools() : fileTools;
    }

    private static final Logger logger = Logger.getLogger("FileRenameTool");

    public static boolean isDir(Object path) {
        if (path instanceof String) {
            Path inPath = Paths.get((String) path);
            return inPath.toFile().isDirectory();
        }
        if (path instanceof Path) {
            return ((Path) path).toFile().isDirectory();
        }
        if (path instanceof File) {
            return ((File) path).isDirectory();
        } else {
            return false;
        }

    }

    /**
     * 修改单个文件名或目录
     *
     * @param srcPath 原文件路径
     * @param dstPath 新文件路径
     * @return 修改成本，则返回ture；否则false
     */
    public static boolean move(String srcPath, String dstPath) {
        // 创建File对象
        File oldFile = new File(srcPath);
        // 修改文件名
        File newFile = new File(dstPath);
        boolean flag = oldFile.renameTo(newFile);
        if (flag)
            logger.log(Level.INFO, "修改文件名成功");
        else
            logger.log(Level.INFO, "修改文件名失败");
        return oldFile.renameTo(newFile);
    }

    /**
     * @param parentPath 父目录
     * @return 返回目录下的所有文件或子文件夹
     */
    public static File[] getChildPath(String parentPath) {
        File folder = new File(parentPath);
        return folder.listFiles();
    }

    /**
     * @param parentPath 父目录
     * @return 返回目录下的所有文件（过滤了子文件夹）
     */
    public static File[] getChildFiles(String parentPath) {
        File folder = new File(parentPath);
        return folder.listFiles(
                new FileFilter() {
                    @Override
                    public boolean accept(File pathname) {
                        return pathname.isFile();
                    }
                }
        );
    }

    /**
     * @param parentPath 父目录
     * @return 遍历父目录及子目录，返回所有的文件
     */
    public static List<String> getALLChildFiles(String parentPath) throws IOException {//只看文件
        Path folder = Paths.get(parentPath);
        List<String> list = new ArrayList<>();
        try (Stream<Path> paths = Files.walk(folder)) {
            paths.filter(Files::isRegularFile).forEach(
                    path -> list.add(path.getParent().toString() + "/" + path.getFileName())
            );
        }
        return list;
    }

    /**
     * 重命名文件，目的是去掉空白字符
     *
     * @param file 原文件名
     * @return 去掉空白字符是否成功
     * 1）输入的是目录，则返回true
     * 2）文件不包含空白字符，则返回true
     * 3）rename成本，则返回true；否则false
     */
    private static boolean removeWhitespace(File file) {
        if (file.isDirectory()) {
            System.out.println("it is dir ===" + file.getAbsolutePath());
            return true;
        }
        if (!file.getName().matches(".*\\s+.*")) {
            System.out.println("file is fine ===" + file.getName());
            return true;
        }
        String dir = file.getParent();
        String fileName = file.getName().replaceAll("\\s+", "");
        System.out.println("file need rename ===" + file.getName());
        return file.renameTo(new File(dir + "/" + fileName));
    }

    public static boolean removeWhitespace(File path, String mode) {
        if (mode.equals("recurse")) {
            if (path.isFile()) {
                return removeWhitespace(path);
            }
            for (File file : Objects.requireNonNull(path.listFiles())) {
                if (file.isDirectory()) {
                    removeWhitespace(file, mode);
                } else {
                    boolean b = removeWhitespace(file);
                    if (!b) {
                        System.out.println("failed ===" + file.getName());
                    }
                }
            }
        }
        if (mode.equals("all")) {
            if (!path.isDirectory()) {
                return true;
            }
            for (File file : Objects.requireNonNull(path.listFiles())) {
                removeWhitespace(file);
            }

        }
        return false;

    }

    public static void main(String[] args) {
        String path = "/Users/dobbin/Downloads/test/";
        removeWhitespace(new File(path), "recurse");
    }
}