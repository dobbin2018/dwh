package example;


import org.example.compress.FileTools;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

public class FileToolsTest {
    @Test
    public void moveTest() {
        FileTools instance = FileTools.getInstance();

        String oldFileName = "/Users/dobbin/.ww/test01.txt";
        String newFileName = "/Users/dobbin/.ww/test01.txt";
        instance.move(oldFileName,newFileName);
    }
    @Test
    public void getChildPathTest() throws IOException {
        FileTools instance = FileTools.getInstance();
        String path = "/Users/dobbin/develop/tmp";
        for (File i:instance.getChildPath(path)){
            System.out.println("===1=== "+i.getAbsolutePath());
        }
        for (File i:instance.getChildFiles(path)){
            System.out.println("===2=== "+i.getAbsolutePath());
        }
        for (String i:instance.getALLChildFiles(path)){
            System.out.println("===3=== "+i);

        }
    }
    @Test
    public void isDirTest(){
        String path ="/Users/dobbin";

        System.out.println(path+(FileTools.isDir(path)?" is a dir":" is not a dir."));;
        path ="/Users/dobbin/develop/code/003/dwh/willy/wtools/RenameLocalFlie/src/test/java/example/FileToolsTest.java";
        System.out.println(path+(FileTools.isDir(path)?" is a dir":" is not a dir."));;
    }
}