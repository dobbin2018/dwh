import hashlib

print(hashlib.md5(str('123213').encode()).hexdigest())


def md5decrypt(num: str):
    with open('md5.txt', 'r') as f:
        result = 0
        for line in f:
            line = line.split('\n')
            if num == line[0]:
                print('解密成功，密文为：', result)
                return result
            else:
                result += 1
        print('解密失败！')
        return None


if __name__ == '__main__':
    md5decrypt('d3e4029427aa766ffa42fd454b28d3b4')