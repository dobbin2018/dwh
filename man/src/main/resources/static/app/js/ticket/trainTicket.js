App.controller('TrainTicketController', ['$scope', '$resource','$http',
    function($scope, $resource,$http) {
        'use strict';
        $scope.pageSize = 10
        $scope.maxSize = 20;
        $scope.bigTotalItems = 0;
        $scope.bigCurrentPage = 1;

        // id,date,time_begin,station_begin,station_end,cheCi,pici,updat_time,uuid,status

        $scope.ticket = {
            "id":"",
            "pici":"",
            "status":""
        }
        $scope.pageChanged = function(){
            console.log($scope.bigCurrentPage)
        }

        $scope.checkTicket = function(_ticket){
            if(_ticket == undefined ){
                _ticket = $scope.ticket
            }
            $http({
                "method": 'POST',
                "url": '/man/mycode/ticket/getCount',
                "data":_ticket
            }).then(
                function successCallback(value) {
                    console.log(",,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,")
                    console.log(value)
                    if (value.data > 0){
                        $scope.bigTotalItems = value.data
                        $scope.getAll( _ticket,$scope.bigCurrentPage,$scope.pageSize)
                    }
                },function errorCallback(reason) {
                    console.log(reason)
                })
        }

        $scope.getAll = function(_data,_pageNum,_pageSize){
            console.log("---------------------------")
            console.log(_pageNum)
            console.log(_pageSize)
            console.log(_data)
            _data.startRow = (_pageNum - 1 )* _pageSize
            _data.pageSize = _pageSize
            console.log(_data)
            $http({
                "method": 'POST',
                "url": '/man/mycode/ticket/getAll',
                "data":  _data
            }).then(
                function successCallback(value) {
                    console.log("aaaaaaaaaaaaaa")
                    console.log(value)
                    if (value.data.length > 0){
                        $scope.list_info = value.data
                    }
                },function errorCallback(reason) {
                    console.log(reason)
                    console.log("pppppppppppppppppppppppppppppp")
                })
        }
        $scope.init = function(){
            $scope.checkTicket()
        }
        $scope.init()

    }]);