// $http.get('server/datatable.json')
//     .success(function (data) {
//     console.log(data);
//         $scope.persons = data;
// });
var app = angular.module('app',['ui.bootstrap']);
app.controller('PathController', ['$scope','$rootScope', '$resource','$http','$modal',
    function($scope,$rootScope, $resource,$http,$modal) {
        'use strict';
     
        $scope.pageSize = 10
        $scope.maxSize = 20;
        $scope.bigTotalItems = 0;
        $scope.bigCurrentPage = 1;

        $scope.dictPath_v = {
            "path_name":"",
            "path_path":"",
            "path_avable":"",
            "update_time":"",
            "update_user":"",
            "path_type":""
        }

        $scope.checkDictpath = function(_dictPath){
            if (_dictPath == undefined){
                _dictPath = $scope.dictPath_v
            }
            _dictPath.path_avable = '1'
            $http({
                "method": 'POST',
                "url": '/man/mycode/path/getCount',
                "data":_dictPath
            }).then(
                function successCallback(value) {
                    console.log(value)
                    if (value.data > 0){
                        $scope.bigTotalItems = value.data
                        $scope.getpath(_dictPath,$scope.bigCurrentPage,$scope.pageSize)
                    }else{
                        $scope.dictpath = {}
                    }
                    console.log("---------------------")
                },function errorCallback(reason) {
                    alert(reason)
                })
        }

        $scope.getpath = function(_data,_pageNum,_pageSize){
            _data.pageNum = _pageNum
            _data.pageSize = _pageSize
            $http({
                "method": 'POST',
                "url": '/man/mycode/path/getAllDictByPage',
                "data":_data
            }).then(function successCallback(value) {
                    console.log(value)
                    if (value.data.length > 0){
                        $scope.dictpath = value.data
                    }
                },
                function errorCallback(reason) {
                    console.log(reason)
                })
        }
        $scope.checkDictpath()



        // $scope.getNews()
        $scope.addDict = function (_d) {
            console.log("新增")
            $scope.modelHtml("addPathDictCtrl")
        }

        // 弹出框
        $scope.modelHtml = function (_ctrl,_data) {
            var modalInstance = $modal.open({
                templateUrl: 'addPathDict.html',
                controller: _ctrl,
                resolve : {
                    data : function() {//data作为modal的controller传入的参数
                        return _data;//用于传递数据
                    }
                }
            });
            console.log("fffffffffffffffffffffff")
            console.log(modalInstance)

            modalInstance.result.then(function (data_flag) {
                console.log("fffs")
                if(data_flag){
                    console.log(data_flag)
                    $scope.checkDictpath()
                }
            })
        }

    }]);


//  新增
app.controller('addPathDictCtrl', function($scope, $modalInstance,data,$http) {
    $scope.data= data;
    console.log(data)
    $scope.ok = function (_msg) {
        $modalInstance.close(_msg);
    };
    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
    // $scope.dictNews = {}
    //  保存数数据
    $scope.saveDictPath = function(_d){
        console.log(_d)
        var url = "save"
        $http({
            "method": 'POST',
            "url": '/man/mycode/path/save',
            "data":_d
        }).then(function successCallback(value) {
            $scope.flag = value.data
            if($scope.flag){
                $scope.ok($scope.flag)
            }else {
                alert("新增失败")
            }
        },function errorCallback(reason) {
            console.log(reason)
            alert(reason.data.message)
        })
    }
});