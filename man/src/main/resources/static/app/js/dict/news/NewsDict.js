// $http.get('server/datatable.json')
//     .success(function (data) {
//     console.log(data);
//         $scope.persons = data;
// });
var app = angular.module('app',['ui.bootstrap']);
app.controller('NewsDictController', ['$scope','$rootScope', '$resource','$http','$modal',
    function($scope,$rootScope, $resource,$http,$modal) {
        'use strict';
        $scope.checkDictNews = function(){
            $http({
                "method": 'POST',
                "url": '/man/mycode/news/getDictCount',
                "data":{}
            }).then(
                function successCallback(value) {
                    console.log(value)
                    if (value.data > 0){
                        $scope.getNews(  )
                    }
                    console.log("---------------------")
                },function errorCallback(reason) {
                    console.log(reason)
                })
        }


        $scope.getNews = function(){
            $http({
                "method": 'POST',
                "url": '/man/mycode/news/getDict',
                "data":{}
            }).then(function successCallback(value) {
                $scope.dictInfo = value.data
                },
                function errorCallback(reason) {
                    console.log(reason)
                })
        }
        $scope.checkDictNews()

        //修改
        $scope.editDict = function(_d){
            $scope.modelHtml("editNewsDictCtrl",_d)
        }

        //删除
        $scope.delDict = function(_d){
            console.log(_d)
        }

        // $scope.getNews()
        $scope.addDict = function (_d) {
            console.log("新增")
            $scope.modelHtml("addNewsDictCtrl")
        }

        // 弹出框
        $scope.modelHtml = function (_ctrl,_data) {
            var modalInstance = $modal.open({
                templateUrl: 'addNewsDict.html',
                controller: _ctrl,
                resolve : {
                    data : function() {//data作为modal的controller传入的参数
                        return _data;//用于传递数据
                    }
                }
            });
            console.log("fffffffffffffffffffffff")
            console.log(modalInstance)

            modalInstance.result.then(function (data_flag) {
                console.log("fffs")
                if(data_flag){
                    console.log(data_flag)
                    $scope.checkDictNews()
                }
            })
        }
    }]);
//  修改
app.controller('editNewsDictCtrl', function($scope, $modalInstance,data,$http) {
    $scope.dictNews= data;

    //  修改成功
    $scope.ok = function (_msg) {
        $modalInstance.close(_msg);
    };
    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
    //  保存数数据
    $scope.saveDictNews = function(_d){
        console.log(_d)
        var url = "save"
        $http({
            "method": 'POST',
            "url": '/man/mycode/news/update',
            "data":_d
        }).then(function successCallback(value) {
                $scope.flag = value.data
              if($scope.flag){
                  $scope.ok($scope.flag)
              }else{
                  alert("修改失败！")
              }
            },function errorCallback(reason) {
                alert("修改失败！"+reason)
                console.log(reason)
            })
    }
});

//  新增
app.controller('addNewsDictCtrl', function($scope, $modalInstance,data,$http) {
    $scope.data= data;
    console.log(data)
    $scope.ok = function (_msg) {
        $modalInstance.close(_msg);
    };
    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
    $scope.dictNews = {}
    //  保存数数据
    $scope.saveDictNews = function(_d){
        console.log(_d)
        var url = "save"
        $http({
            "method": 'POST',
            "url": '/man/mycode/news/save',
            "data":_d
        }).then(function successCallback(value) {
                $scope.flag = value.data
               if($scope.flag){
                   $scope.ok($scope.flag)
               }else {
                   alert("新增失败")
               }
            },function errorCallback(reason) {
                console.log(reason)
                alert(reason.data.message)
            })
    }
});