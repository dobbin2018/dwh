// $http.get('server/datatable.json')
//     .success(function (data) {
//     console.log(data);
//         $scope.persons = data;
// });
var app = angular.module('app',['ui.bootstrap']);
app.controller('ScriptController', ['$scope','$rootScope', '$resource','$http','$modal',
    function($scope,$rootScope, $resource,$http,$modal) {
        'use strict';
        // 时间

        $scope.today = function() {
            $scope.dt = new Date();
        };
        $scope.today();

        $scope.clear = function () {
            $scope.dt = null;
        };

        // Disable weekend selection
        $scope.disabled = function(date, mode) {
            return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
        };

        $scope.toggleMin = function() {
            $scope.minDate = $scope.minDate ? null : new Date();
        };
        $scope.toggleMin();

        $scope.open = function($event) {
            $event.preventDefault();
            $event.stopPropagation();

            $scope.opened = true;
        };

        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 1
        };

        $scope.initDate = new Date('2016-15-20');
        $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
        $scope.format = $scope.formats[0];


        $scope.pageSize = 10
        $scope.maxSize = 20;
        $scope.bigTotalItems = 0;
        $scope.bigCurrentPage = 1;

        $scope.dictScript_v = {
            "script_name":"",
            "script_path":"",
            "script_avable":"",
            "update_time":"",
            "update_user":"",
            "script_type":""
        }

        $scope.checkDictScript = function(_dictScript){
            if (_dictScript == undefined){
                _dictScript = $scope.dictScript_v
            }
            $http({
                "method": 'POST',
                "url": '/man/mycode/script/getCount',
                "data":_dictScript
            }).then(
                function successCallback(value) {
                    console.log(value)
                    if (value.data > 0){
                        $scope.bigTotalItems = value.data
                        $scope.getScript(_dictScript,$scope.bigCurrentPage,$scope.pageSize)
                    }else{
                        $scope.dictScript = {}
                    }
                    console.log("---------------------")
                },function errorCallback(reason) {
                    alert(reason)
                })
        }

        $scope.getScript = function(_data,_pageNum,_pageSize){
            _data.pageNum = _pageNum
            _data.pageSize = _pageSize
            $http({
                "method": 'POST',
                "url": '/man/mycode/script/getAllScript',
                "data":_data
            }).then(function successCallback(value) {
                    console.log(value)
                    if (value.data.length > 0){
                        $scope.dictScript = value.data
                    }
                },
                function errorCallback(reason) {
                    console.log(reason)
                })
        }
        $scope.checkDictScript()

    }]);