App
    .run([
        '$rootScope','$state','$stateParams','$location','$cookieStore',
        function ($rootScope,$state,$stateParams,$location,$cookieStore) {

                $rootScope.$on('$stateChangeStart',
                    function(event, toState, toParams, fromState, fromParams,$cookieStore) {
                    if( toState.name == 'login') return  // 如果是进入登录界面则允许
                    // if ( !$rootScope.user ){
                        console.log("0000000000000000000000000000000000000000000000")
                    if (  $rootScope.userInfo ){
                        console.log($rootScope.userInfo )
                        console.log("11111111111111111111111111111111")
                        // event.preventDefault();// 取消默认跳转行为
                        // $state.go("/login",{from:fromState.name,w:'notLogin'});//跳转到登录界面
                        // $state.go("dashboard");//跳转到登录界面
                      // $templateCache.remove(toState.templateUrl);
                    }else{
                        $state.go("login");//跳转   欢迎界面
                    }
                });
    //         //根据  用户的id 判断用户
    //         var user_info = $cookieStore.get("user_info")
    //
    //         if (user_info != undefined ){
    //             var cookiearr = $cookieStore.get("user_info")
    //             var cookieUserInfo = angular.fromJson(cookiearr)
    //
    //             $cookieStore.put("userInfo",cookieUserInfo) // cookie中 保存  用户信息
    //         }
    //         $rootScope.$on('stateCtr',function (event, toState, toParams, fromState, fromParams, $location) {
    //             if (toState.name == "login") {
    //                 return
    //             }
    //
    //             $sookieStore.userInfo =  $sookieStore.get("userInfo")
    //         })
        }
    ])
    .config(['$stateProvider', '$locationProvider', '$urlRouterProvider', 'RouteHelpersProvider',
    function ($stateProvider, $locationProvider, $urlRouterProvider, helper) {
        'use strict';

        // Set the following to true to enable the HTML5 Mode
        // You may have to set <base> tag in index and a routing configuration in your server
        $locationProvider.html5Mode(false);


        // defaults to dashboard
        $urlRouterProvider.otherwise('/login');

        //
        // Application Routes
        // -----------------------------------
        $stateProvider
            .state('app', {
                url: '/app',
                abstract: true,
                templateUrl: helper.basepath('app.html'),
                controller: 'AppController',
                resolve: helper.resolveFor('fastclick', 'modernizr', 'icons', 'screenfull', 'animo', 'sparklines', 'slimscroll', 'classyloader', 'toaster', 'whirl')
            })

            .state('app.ticket_trainTicket', {
                url: '/ticket_ticket_trainTicket',
                templateUrl: 'app/views/ticket/trainTicket.html',
                resolve: {
                    deps: ['$ocLazyLoad',
                        function( $ocLazyLoad){
                            // helper.resolveFor('datatables')
                            return $ocLazyLoad.load([
                                'app/js/ticket/trainTicket.js',
                                'datatables'
                            ]);
                        }]
                }
            })
            .state('app.upload_createtableee', {
                url: '/upload_createtables',
                templateUrl: 'app/views/upload/createtable/createtable.html',
                resolve:helper.resolveFor('angularFileUpload','filestyle')
            })
            .state('app.dashboard', {
                url: '/dashboard',
                title: '系统',
                templateUrl: helper.basepath('dashboard.html'),
                resolve: helper.resolveFor('flot-chart','flot-chart-plugins')
            })
            .state('login', {
                url: '/login',
                title: "Login",
                templateUrl: 'login.html',
                resolve: {
                    deps: ['$ocLazyLoad',
                        function( $ocLazyLoad){
                            // helper.resolveFor('datatables')
                            return $ocLazyLoad.load([
                                'app/js/login.js'
                            ]);
                        }]
                }
            })

            .state('app.newsBatch', {
                url: '/newsBatch',
                templateUrl: 'app/views/news/batchNews.html',
                resolve: {
                    deps: ['$ocLazyLoad',
                        function( $ocLazyLoad){
                            // helper.resolveFor('datatables')
                            return $ocLazyLoad.load([
                                'app/js/news/batchNews.js',
                                'datatables'
                            ]);
                        }]
                }
            })

            .state('app.ngdialog', {
                url: '/ngdialog',
                title: 'ngDialog',
                templateUrl: helper.basepath('ngdialog.html'),
                resolve: angular.extend(helper.resolveFor('ngDialog'),{
                    tpl: function() { return { path: helper.basepath('app/views/news/batchNews.html') }; }
                }),
                controller: 'DialogIntroCtrl'
            })

            // 新闻
            .state('app.table-standard', {
                url: '/table-standard',
                title: 'Table Standard',
                templateUrl: helper.basepath('table-standard.html')
            })
            .state('app.table-extended', {
                url: '/table-extended',
                title: 'Table Extended',
                templateUrl: helper.basepath('table-extended.html')
            })
            .state('app.table-datatable', {
                url: '/table-datatable',
                title: 'Table Datatable',
                templateUrl: helper.basepath('table-datatable.html'),
                resolve: helper.resolveFor('datatables')
            })
            .state('app.table-xeditable', {
                url: '/table-xeditable',
                templateUrl: helper.basepath('table-xeditable.html'),
                resolve: helper.resolveFor('xeditable')
            })
            .state('app.table-ngtable', {
                url: '/table-ngtable',
                templateUrl: helper.basepath('table-ngtable.html'),
                resolve: helper.resolveFor('ngTable', 'ngTableExport')
            })
            .state('app.table-nggrid', {
                url: '/table-nggrid',
                templateUrl: helper.basepath('table-ng-grid.html'),
                resolve: helper.resolveFor('ngGrid')
            })
            .state('app.table-uigrid', {
                url: '/table-uigrid',
                templateUrl: helper.basepath('table-uigrid.html'),
                resolve: helper.resolveFor('ui.grid')
            })
            .state('app.table-angulargrid', {
                url: '/table-angulargrid',
                templateUrl: helper.basepath('table-angulargrid.html'),
                resolve: helper.resolveFor('angularGrid')
            })

            .state('app.news_todayNews', {
                url: '/news_todayNews',
                templateUrl: 'app/views/news/todayNews.html',
                resolve: {
                    deps: ['$ocLazyLoad',
                        function( $ocLazyLoad){
                            // helper.resolveFor('datatables')
                            return $ocLazyLoad.load([
                                'app/js/news/todayNews.js',
                                'datatables'
                            ]);
                        }]
                }
            })
            .state('app.dict_news', {
                url: '/dict_news',
                templateUrl: 'app/views/dict/news/NewsDict.html',
                resolve: {
                    deps: ['$ocLazyLoad',
                        function( $ocLazyLoad){
                            // helper.resolveFor('datatables')
                            return $ocLazyLoad.load([
                                'app/js/dict/news/NewsDict.js',
                                'datatables'
                            ]);
                        }]
                }
            })
            .state('app.dict_script', {
                url: '/dict_script',
                templateUrl: 'app/views/dict/script/Script.html',
                resolve: {
                    deps: ['$ocLazyLoad',
                        function( $ocLazyLoad){
                            return $ocLazyLoad.load([
                                'app/js/dict/script/Script.js',
                                'datatables'
                            ]);
                        }]
                }
            })
            .state('app.dict_path', {
                url: '/dict_path',
                templateUrl: 'app/views/dict/path/Path.html',
                resolve: {
                    deps: ['$ocLazyLoad',
                        function( $ocLazyLoad){
                            return $ocLazyLoad.load([
                                'app/js/dict/path/Path.js',
                                'datatables'
                            ]);
                        }]
                }
            })
            //文件分拣
            .state('app.file_router', {
                url: '/file_router',
                templateUrl: 'app/views/file/file.html',
                resolve: {
                    deps: ['$ocLazyLoad',
                        function( $ocLazyLoad){
                            return $ocLazyLoad.load([
                                'app/js/file/file.js',
                                'datatables'
                            ]);
                        }]
                }
            })
            .state('app.file_python', {
                url: '/file_python',
                templateUrl: 'app/views/file/file_python.html',
                resolve: {
                    deps: ['$ocLazyLoad',
                        function( $ocLazyLoad){
                            return $ocLazyLoad.load([
                                'app/js/file/file_python.js',
                                'datatables'
                            ]);
                        }]
                }
            })
            .state('app.file_shell', {
                url: '/file_shell',
                templateUrl: 'app/views/file/file_shell.html',
                resolve: {
                    deps: ['$ocLazyLoad',
                        function( $ocLazyLoad){
                            return $ocLazyLoad.load([
                                'app/js/file/file_shell.js',
                                'datatables'
                            ]);
                        }]
                }
            })
            .state('app.file_doc', {
                url: '/file_doc',
                templateUrl: 'app/views/file/file_doc.html',
                resolve: {
                    deps: ['$ocLazyLoad',
                        function( $ocLazyLoad){
                            return $ocLazyLoad.load([
                                'app/js/file/file_doc.js',
                                'datatables'
                            ]);
                        }]
                }
            })
            .state('app.file_excel', {
                url: '/file_excel',
                templateUrl: 'app/views/file/file_excel.html',
                resolve: {
                    deps: ['$ocLazyLoad',
                        function( $ocLazyLoad){
                            return $ocLazyLoad.load([
                                'app/js/file/file_excel.js',
                                'datatables'
                            ]);
                        }]
                }
            })
            .state('app.file_html', {
                url: '/file_html',
                templateUrl: 'app/views/file/html_file.html',
                resolve: {
                    deps: ['$ocLazyLoad',
                        function( $ocLazyLoad){
                            return $ocLazyLoad.load([
                                'app/js/file/html_file.js',
                                'datatables'
                            ]);
                        }]
                }
            })
            .state('app.file_txt', {
                url: '/file_txt',
                templateUrl: 'app/views/file/txt_file.html',
                resolve: {
                    deps: ['$ocLazyLoad',
                        function( $ocLazyLoad){
                            return $ocLazyLoad.load([
                                'app/js/file/txt_file.js',
                                'datatables'
                            ]);
                        }]
                }
            })

            .state('app.news.hotNews', {
                url: '/news.hotNews',
                title: 'Form Standard',
                templateUrl: helper.basepath('hotNews.html'),
                resolve: helper.resolveFor('datatables'),
            })
            //  新闻
            .state('app.code-editor', {
                url: '/code-editor',
                templateUrl: helper.basepath('code-editor.html'),
                resolve: {
                    deps: helper.resolveFor('codemirror',
                        'ui.codemirror', 'codemirror-modes-web', 'angularBootstrapNavTree').deps,
                    filetree: ["LoadTreeService", function (LoadTreeService) {
                        return LoadTreeService.get().$promise.then(function (res) {
                            return res.data;
                        });
                    }]
                },
                controller: ["$rootScope", "$scope", "filetree", function($rootScope, $scope, filetree) {
                    // Set the tree data into the scope
                    $scope.filetree_data = filetree;
                    // Setup the layout mode
                    $rootScope.app.useFullLayout = true;
                    $rootScope.app.hiddenFooter = true;
                    $rootScope.app.layout.isCollapsed = true;
                    // Restore layout
                    $scope.$on('$destroy', function(){
                        $rootScope.app.useFullLayout = false;
                        $rootScope.app.hiddenFooter = false;
                    });
                }]
            })
            .state('app.template', {
                url: '/template',
                title: 'Blank Template',
                templateUrl: helper.basepath('template.html')
            })
            .state('app.documentation', {
                url: '/documentation',
                title: 'Documentation',
                templateUrl: helper.basepath('documentation.html'),
                resolve: helper.resolveFor('flatdoc')
            })
            // Forum
            // -----------------------------------
            .state('app.forum', {
                url: '/forum',
                title: 'Forum',
                templateUrl: helper.basepath('forum.html')
            })
            .state('app.forum-topics', {
                url: '/forum/topics/:catid',
                title: 'Forum Topics',
                templateUrl: helper.basepath('forum-topics.html')
            })
            .state('app.forum-discussion', {
                url: '/forum/discussion/:topid',
                title: 'Forum Discussion',
                templateUrl: helper.basepath('forum-discussion.html')
            })
            // Blog
            // -----------------------------------
            .state('app.blog', {
                url: '/blog',
                title: 'Blog',
                templateUrl: helper.basepath('blog.html'),
                resolve: helper.resolveFor('angular-jqcloud')
            })
            .state('app.blog-post', {
                url: '/post',
                title: 'Post',
                templateUrl: helper.basepath('blog-post.html'),
                resolve: helper.resolveFor('angular-jqcloud')
            })
            .state('app.articles', {
                url: '/articles',
                title: 'Articles',
                templateUrl: helper.basepath('blog-articles.html'),
                resolve: helper.resolveFor('datatables')
            })
            .state('app.article-view', {
                url: '/article/:id',
                title: 'Article View',
                templateUrl: helper.basepath('blog-article-view.html'),
                resolve: helper.resolveFor('ui.select', 'textAngular')
            })
            // eCommerce
            // -----------------------------------
            .state('app.orders', {
                url: '/orders',
                title: 'Orders',
                templateUrl: helper.basepath('ecommerce-orders.html'),
                resolve: helper.resolveFor('datatables')
            })
            .state('app.order-view', {
                url: '/order-view',
                title: 'Order View',
                templateUrl: helper.basepath('ecommerce-order-view.html')
            })
            .state('app.products', {
                url: '/products',
                title: 'Products',
                templateUrl: helper.basepath('ecommerce-products.html'),
                resolve: helper.resolveFor('datatables')
            })
            .state('app.product-view', {
                url: '/product/:id',
                title: 'Product View',
                templateUrl: helper.basepath('ecommerce-product-view.html')
            })

            //
            // Horizontal layout
            // -----------------------------------
            .state('app-h', {
                url: '/app-h',
                abstract: true,
                templateUrl: helper.basepath( 'app-h.html' ),
                controller: 'AppController',
                resolve: helper.resolveFor('fastclick', 'modernizr', 'icons', 'screenfull', 'animo', 'sparklines', 'slimscroll', 'classyloader', 'toaster', 'whirl')
            })
            .state('app-h.dashboard_v2', {
                url: '/dashboard_v2',
                title: 'Dashboard v2',
                templateUrl: helper.basepath('dashboard_v2.html'),
                controller: ["$rootScope", "$scope", function($rootScope, $scope) {
                    $rootScope.app.layout.horizontal = true;
                    $scope.$on('$destroy', function(){
                        $rootScope.app.layout.horizontal = false;
                    });
                }],
                resolve: helper.resolveFor('flot-chart','flot-chart-plugins')
            })

        ;
    }]);
