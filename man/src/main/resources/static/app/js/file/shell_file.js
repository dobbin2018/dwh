App.controller('SHELL_FileController', ['$scope', '$resource','$http',
    function($scope, $resource,$http) {
        'use strict';
        $scope.pageSize = 10
        $scope.maxSize = 20;
        $scope.bigTotalItems = 0;
        $scope.bigCurrentPage = 1;

        $scope.todayNewsModel = {
            "news_basic_net_name":"",
            "a_text":"",
            "update_time":""
        }
        $scope.pageChanged = function(){
            console.log($scope.bigCurrentPage)
        }

        $scope.getCount = function(_todayNewsModel){
            if(_todayNewsModel == undefined ){
                _todayNewsModel = $scope.todayNewsModel
            }
            $http({
                "method": 'POST',
                "url": '/man/mycode/file/getCount',
                "data":_todayNewsModel
            }).then(
                function successCallback(value) {
                    console.log(value)
                    if (value.data > 0){
                        // $scope.filesInfo = value.data
                        console.log( $scope.filesInfo )
                        $scope.getFiles( _todayNewsModel,$scope.bigCurrentPage,$scope.pageSize)
                    }
                },function errorCallback(reason) {
                    console.log(reason)
                })
        }

        $scope.getFiles = function(_data,_pageNum,_pageSize){
            console.log(_data)
            _data.pageNum = _pageNum
            _data.pageSize = _pageSize
            $http({
                "method": 'POST',
                "url": '/man/mycode/file/getFile',
                "data":  _data
            }).then(
                function successCallback(value) {
                    console.log(value)
                    if (value.data.length > 0){
                        $scope.filesInfo = value.data
                        // $scope.newsInfo = value.data
                    }
                },function errorCallback(reason) {
                    console.log(reason)
                    console.log("pppppppppppppppppppppppppppppp")
                })
        }
        $scope.init = function(){
            $scope.getCount()
        }
        $scope.init()

    }]);