App.controller('TXT_FileController', ['$scope', '$resource','$http',
    function($scope, $resource,$http) {
        'use strict';
        $scope.pageSize = 100
        $scope.maxSize = 20;
        $scope.bigTotalItems = 0;
        $scope.bigCurrentPage = 1;

        $scope.filesInfo = []

        $scope.getCount = function(){
            var _d = {
                "analysed":"true",
                "fileType":".txt",
                "toDb":"false"
            }
            $http({
                "method": 'POST',
                "url": '/man/mycode/file/getCount',
                "data":_d
            }).then(
                function successCallback(value) {
                    console.log(value)
                    if (value.data >= 0){
                        $scope.bigTotalItems = value.data
                        $scope.getFiles(_d,$scope.bigCurrentPage,$scope.pageSize)
                    }
                },function errorCallback(reason) {
                    console.log(reason)
                })
        }

        $scope.getFiles = function(_data,_pageNum,_pageSize){
            _data.pageNum = _pageNum
            _data.pageSize = _pageSize
            $http({
                "method": 'POST',
                "url": '/man/mycode/file/getFile',
                "data":  _data
            }).then(
                function successCallback(value) {
                    console.log(value)
                    if (value.data.length >= 0){
                        $scope.filesInfo = value.data
                    }
                },function errorCallback(reason) {
                    console.log(reason)
                })
        }

        $scope.all2DB = function(){
            if($scope.filesInfo.length > 0){
                $scope.multiToDb($scope.filesInfo)
            }


        }

        //  多文件入库
        $scope.multiToDb = function(_fileList){
            for(var i = 0 ; i < _fileList.length ; i++){
                $http({
                    "method": 'POST',
                    "url": '/man/mycode/news/saveNews',
                    "data":  _fileList[i]
                }).then(
                    function successCallback(value) {
                        console.log(value)
                        if (value.data = true){
                        }
                    },function errorCallback(reason) {
                        console.log(reason)
                        console.log("pppppppppppppppppppppppppppppp")
                    })
            }
            $scope.getCount()
        }

        //  入库
        $scope.toDB = function(_file){
            console.log(_file)
            $http({
                "method": 'POST',
                "url": '/man/mycode/news/saveNews',
                "data":  _file
            }).then(
                function successCallback(value) {
                    console.log(value)
                    if (value.data = true){
                        $scope.getCount()
                    }
                },function errorCallback(reason) {
                    alert(reason)
                    console.log(reason)
                    console.log("pppppppppppppppppppppppppppppp")
                })
        }


        $scope.init = function(){
            $scope.getCount()
        }
        $scope.init()

    }]);