App.controller('FileController', ['$scope', '$resource','$http','$cookies','$cookieStore',
    function($scope, $resource,$http,$cookies,$cookieStore) {
        'use strict';
        $scope.pageSize = 100
        $scope.maxSize = 20;
        $scope.bigTotalItems = 0;
        $scope.bigCurrentPage = 1;

        // $scope.selectPath={"name":"","path":""}
        // 查询  存在的路径
        $scope.pathList = []
        $scope.filesInfo = []

        $scope.pageChanged = function(){
            console.log($scope.bigCurrentPage)
        }

        $scope.getCount = function(){
            var _d = {
                "analysed":"false",
                "fileType":"false"
            }
            $http({
                "method": 'POST',
                "url": '/man/mycode/file/getCount',
                "data":_d
            }).then(
                function successCallback(value) {
                    console.log(value)
                    if (value.data >= 0){
                        $scope.bigTotalItems = value.data
                        console.log("yyyyyyyyyyyyyyyyyyyyyy")
                        console.log($scope.bigTotalItems)
                        $scope.getFiles(_d,$scope.bigCurrentPage,$scope.pageSize)
                    }
                },function errorCallback(reason) {
                    console.log(reason)
                })
        }

        $scope.getFiles = function(_data,_pageNum,_pageSize){
            _data.pageNum = _pageNum
            _data.pageSize = _pageSize
            $http({
                "method": 'POST',
                "url": '/man/mycode/file/getFile',
                "data":  _data
            }).then(
                function successCallback(value) {
                    console.log(value)
                    if (value.data.length >= 0){
                        $scope.filesInfo = value.data
                    }
                },function errorCallback(reason) {
                    console.log(reason)
                })
        }

        //  查询路径
        $scope.getpath = function(){
            $http({
                "method": 'POST',
                "url": '/man/mycode/path/getAllDict',
                "data":{}
            }).then(
                function successCallback(value) {
                    if (value.data.length >= 0){
                        $scope.dictpath = value.data
                        console.log($scope.dictpath)
                        console.log("============================")
                        for(var i = 0 ; i < value.data.length ; i++){
                            $scope.pathList.push({"name":value.data[i].path_name,"path":value.data[i].path_path})
                        }
                        console.log("--------------------------")
                        console.log($scope.pathList)

                    }
                },
                function errorCallback(reason) {
                    console.log(reason)
                })
        }

        // 全部 分拣
        $scope.fenjianAll = function(){
            console.log("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa")
            console.log($scope.bigTotalItems / $scope.pageSize)
            if($scope.filesInfo.length > 0){
                $scope.multiClassFile($scope.filesInfo)
            }
        }

        // 多文件分类
        $scope.multiClassFile = function(_fileList){
            for(var i = 0 ; i < _fileList.length ; i++){
                var _data = {
                    "oldFile":_fileList[i].oldFile,
                    "fileMD5":_fileList[i].fileMD5
                }
                $http({
                    "method": 'POST',
                    "url": '/man/mycode/file/classFile',
                    "data":_data
                }).then(
                    function successCallback(value) {
                        if (value.data == true){
                            console.log($scope.dictpath)
                        }
                    },
                    function errorCallback(reason) {
                        console.log(reason)
                    })
            }
            $scope.getCount()
        }

        // 单文件 文件分类
        $scope.classFile = function (_file) {
            var _data = {
                "oldFile":_file.oldFile,
                "fileMD5":_file.fileMD5
            }
            $http({
                "method": 'POST',
                "url": '/man/mycode/file/classFile',
                "data":_data
            }).then(
                function successCallback(value) {
                    if (value.data == true){
                        console.log($scope.dictpath)
                        $scope.getCount()
                    }
                },
                function errorCallback(reason) {
                    alert(reason)
                    console.log(reason)
                })
        }
        /**
         * 删除文件
         * @param _d
         */
        $scope.del_file = function(_d){
            console.log("bbbbbbbbbbbbb")
            // console.log($cookies.get(key))
            console.log("mmmmmmmmmmmmmmmmmmm")
            console.log(_d)
            $http({
                "method": 'POST',
                "url": '/man/mycode/file/delFile',
                "data":{
                    "oldFile":_d.oldFile
                }
            }).then(
                function successCallback(value) {
                    if (value.data == true){
                        $scope.dictpath = value.data
                        console.log($scope.dictpath)
                        $scope.getFiles()
                    }
                },
                function errorCallback(reason) {
                    alert(reason)
                    console.log(reason)
                })
        }

         // 点击文件名  打开  操作
        $scope.goTo = function(_d){
            console.log(_d)
            console.log($scope.selectPath.path)
            console.log( $scope.kkk)
        }


        $scope.init = function(){
            $scope.getCount()
            $scope.getpath()
        }
        $scope.init()

    }]);