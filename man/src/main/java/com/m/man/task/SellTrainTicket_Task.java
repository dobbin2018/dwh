package com.m.man.task;

import com.m.man.queue.kafka.KafkaProducerInterface;
import com.m.man.task.service.TrainTicketService;
import com.m.man.util.JsonUtil;
import com.m.man.util.TimeUtil;
import com.m.man.webSocket.util4.WebSocket;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

@Configuration  //1.主要用于标记配置类，兼备Component的效果。
@EnableScheduling
public class SellTrainTicket_Task {

    @Autowired
    private TrainTicketService service;

    @Autowired
    private KafkaProducerInterface kafkaServer;

//    @Scheduled(cron = "0/1 * * * * ?")
//    private void sellTrainTicket(){
//        String json = service.sellTicket();
//        System.out.println("生成销售车票的报文="+json);
//        try {
//            kafkaServer.send("textTopic", json);
////            new WebSocket().onMessage(Object);
//        } catch (ExecutionException e) {
//            e.printStackTrace();
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        } catch (TimeoutException e) {
//            e.printStackTrace();
//        }
//    }


    @Scheduled(cron = "0/10 * * * * ?")
    private void sellTrainOneTicket(){
        String json = oneMineJson();
        System.out.println("生成销售车票的报文="+json);
        try {
            kafkaServer.send("textTopic", json);
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (TimeoutException e) {
            e.printStackTrace();
        }
    }

    @Scheduled(cron = "0/50 * * * * ?")
    private void sellTrainFiveTicket(){
        String json = fiveMineJson();
        System.out.println("生成销售车票的报文="+json);
        try {
            kafkaServer.send("textTopic", json);
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (TimeoutException e) {
            e.printStackTrace();
        }
    }
    /**
     *  生成json  每分钟 1 次
     */
    public String oneMineJson(){
        Map<String, String> m = new HashMap<String, String>();
        m.put("create_time", TimeUtil.timeSSS());
        m.put("create_targ","targ_1");
        return JsonUtil.toJSon(m);
    }

    /**
     *  生成json  每分钟 5 次
     */
    public String fiveMineJson(){
        Map<String, String> m = new HashMap<String, String>();
        m.put("create_time", TimeUtil.timeSSS());
        m.put("create_targ","targ_5");
        return JsonUtil.toJSon(m);
    }
}
