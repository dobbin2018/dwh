package com.m.man.task.po;

public class TrainTicketPO extends TicketPO {
    private String CheCi;  // 车次
    private String status; // 状态

    public TrainTicketPO() {
    }

    public TrainTicketPO(String cheCi, String status) {
        CheCi = cheCi;
        this.status = status;
    }

    public TrainTicketPO(String date, String time_begin, String station_begin, String station_end, String cheCi, String status) {
        super(date, time_begin, station_begin, station_end);
        CheCi = cheCi;
        this.status = status;
    }

    public TrainTicketPO(String id, String create_time, String update_time, String remark, String date, String time_begin, String station_begin, String station_end, String cheCi, String status) {
        super(id, create_time, update_time, remark, date, time_begin, station_begin, station_end);
        CheCi = cheCi;
        this.status = status;
    }

    public String getCheCi() {
        return CheCi;
    }

    public void setCheCi(String cheCi) {
        CheCi = cheCi;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
