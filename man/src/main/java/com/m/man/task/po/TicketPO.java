package com.m.man.task.po;

public class TicketPO  extends Base_Attribute{
    private String date;  //  日期
    private String time_begin;   // 开车时间

    private String station_begin;//出发站
    private String station_end;  //终点站

    public TicketPO() {
    }

    public TicketPO(String id, String create_time, String update_time, String remark, String date, String time_begin, String station_begin, String station_end) {
        super(id, create_time, update_time, remark);
        this.date = date;
        this.time_begin = time_begin;
        this.station_begin = station_begin;
        this.station_end = station_end;
    }

    public TicketPO(String date, String time_begin, String station_begin, String station_end) {
        this.date = date;
        this.time_begin = time_begin;
        this.station_begin = station_begin;
        this.station_end = station_end;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime_begin() {
        return time_begin;
    }

    public void setTime_begin(String time_begin) {
        this.time_begin = time_begin;
    }

    public String getStation_begin() {
        return station_begin;
    }

    public void setStation_begin(String station_begin) {
        this.station_begin = station_begin;
    }

    public String getStation_end() {
        return station_end;
    }

    public void setStation_end(String station_end) {
        this.station_end = station_end;
    }
}
