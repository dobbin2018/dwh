package com.m.man.task.po;

import com.m.man.util.TimeUtil;
import com.m.man.util.UUIDUtil;

/**
 *  事件的 通用属性
 */
public class Base_Attribute {

    private String id = UUIDUtil.getUUID();  //   主键

    private String create_time = TimeUtil.timeSSS(); // 创建  时间
    private String update_time; // 修改  时间
    private String remark     ; // 备注

    public Base_Attribute() {
    }

    public Base_Attribute(String id, String create_time, String update_time, String remark) {
        this.id = id;
        this.create_time = create_time;
        this.update_time = update_time;
        this.remark = remark;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCreate_time() {
        return create_time;
    }

    public void setCreate_time(String create_time) {
        this.create_time = create_time;
    }

    public String getUpdate_time() {
        return update_time;
    }

    public void setUpdate_time(String update_time) {
        this.update_time = update_time;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
