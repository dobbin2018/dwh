package com.m.man.task.service.impl;

import com.m.man.task.po.TrainTicketPO;
import com.m.man.task.service.TrainTicketService;
import com.m.man.util.JsonUtil;
import com.m.man.util.TimeUtil;
import org.springframework.stereotype.Service;

@Service
public class TrainTicketServiceImpl implements TrainTicketService {

    /**
     *  生成报文
     */
    public String sellTicket() {
        TrainTicketPO ticket = SellTicket();  // 生成  一张车票
        String ticket_json_str = JsonUtil.toJSon(ticket);  // 转成  json

        return  ticket_json_str;
    }

    /**
     * 生成  车票  卖出
     */
    private TrainTicketPO  SellTicket(){
        TrainTicketPO trainTicket = new TrainTicketPO();

//        trainTicket.setCheCi("101");
//        trainTicket.setStatus("卖出");
//        trainTicket.setDate(TimeUtil.timeDay());

//        trainTicket.setRemark("这个是备注字段");
//        trainTicket.setStation_begin("北京");
//        trainTicket.setStation_end("南京");

//        trainTicket.setTime_begin(TimeUtil.timeMine());

        return trainTicket ;
    }

}
