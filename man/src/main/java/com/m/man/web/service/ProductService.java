package com.m.man.web.service;

import java.util.List;

import com.m.man.web.dao.IProductDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.m.man.web.model.ProductPO;

@Service
public class ProductService {
	
	@Autowired
	private IProductDao productDao;
	   
	   public List<ProductPO> getAllProducts() {

	   	return productDao.getAllProducts();
	   }

}
