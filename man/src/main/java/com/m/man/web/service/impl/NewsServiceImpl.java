package com.m.man.web.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.m.man.util.FileOptionUtil;
import com.m.man.util.TimeUtil;
import com.m.man.util.UUIDUtil;
import com.m.man.web.dao.NewsDao;
import com.m.man.web.model.FilePO;
import com.m.man.web.model.NewsNetDict;
import com.m.man.web.model.NewsPO;
import com.m.man.web.service.FileService;
import com.m.man.web.service.NewsService;
import com.m.man.web.service.PathService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.UncategorizedSQLException;
import org.springframework.stereotype.Service;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.sql.SQLSyntaxErrorException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class NewsServiceImpl implements NewsService {

    @Value("${UPLOAD.file.home_path}")
    private String home_path ;

    @Autowired
    private NewsDao dao;
    @Autowired
    private PathService pathService;
    @Autowired
    private FileService fileService;

    @Override
    public PageInfo<NewsPO> getAllNews(NewsPO news,Integer pageNum,Integer pageSize) {
        PageHelper.startPage(pageNum,pageSize);
        List<NewsPO> list = dao.getAllNews(news);
        PageInfo<NewsPO> pageInfo = new PageInfo<>(list);
        return pageInfo;
    }

    @Override
    public List<NewsPO> getNews(NewsPO news) {
        List<NewsPO> infos = dao.getNews(news);
        return infos;
    }

    @Override
    public int getCount(NewsPO news) {
        int count = dao.getCount(news);
        return count;
    }

    @Override
    public Map<String, Object> saveNews2(String path) throws UnsupportedEncodingException, FileNotFoundException {
        return null;
    }

    /**
     * 新闻结构化  保存
     * @return
     */
    @Override
    public boolean saveNews(FilePO filePO) throws UnsupportedEncodingException, FileNotFoundException {
        //
        String path = filePO.getOldFile();

        Map<String,Object> m = new HashMap<String, Object>();
        boolean r_flag = true;
            System.out.println("路径=="+path);
            //  读取文件
            String name = new File(path).getName() ; // 文件名
            String data_file_name = name ;//数据 名称
            String [] nameArray = name.split("\\.")[0].split("_");
            String bak_file_name = nameArray[1]+"_"+ nameArray[2]+"_"+ nameArray[3] ;// 备份文件日期
            String news_basic_net_name = nameArray[0] ;//  网站名称
            String news_basic_net_url = "" ; //  网址
            // 读取文件
            System.out.println("---------------------"+name);
            List<String> infoList = FileOptionUtil.readFileLine2List(path);
            r_flag = inDB(infoList,news_basic_net_name,news_basic_net_url,data_file_name);  //  入库 并 记录错误日志
            // 入库成功  更新  文件 记录表的  状态
            if(r_flag == true ){
                FilePO file = new FilePO();
                file.setFileMD5(filePO.getFileMD5());
                file.setToDb("true");
                boolean u_flag  = fileService.updateGetFileToDb(file);
                System.out.println("更新记录结果=="+u_flag);
                if( u_flag == false){
                    r_flag = false;
                }
            }
        m.put("flag",r_flag);
        return r_flag;
    }


    /**
     *  文本入库
     */
    public boolean inDB(List<String> infos,
                        String news_basic_net_name,
                        String news_basic_net_url,
                        String data_file_name
                        )  {
        Boolean r_flag = true;
        for(String info :infos){
            String in_date = TimeUtil.timeSSS();  //  入库时间

            // 分解 每一行的数据
            String [] rowArray = info.split("\\t");
            try{
                String id = rowArray[0];  // id
                String a_text = rowArray[3];
                String a_url = rowArray[4];
                String data_file_time = rowArray[5] ;
                //  入库类
                NewsPO news = new NewsPO();

                news.setId(id);
                news.setNews_basic_net_name(news_basic_net_name);// 网站名
                news.setNews_basic_net_url(news_basic_net_url); // 网址
                news.setUpdate_time(in_date); // 最后修改时间
                news.setIn_date(in_date);  //  录入时间
                news.setIn_man("admin");  //  录入人
                news.setUpdate_man("admin");  //  修改人
                news.setA_text(a_text);//标题
                news.setA_url(a_url);// 标题链接
                news.setData_file_time(data_file_time);  // 文件时间
                news.setData_file_name(data_file_name);//  来源文件名

                // 校验  数据是否否存在
                int flag_find = dao.getCountById(news.getId());
                if(flag_find == 0){  // 数据不存在  准备入库
                    boolean flag_save = dao.saveNews(news);
                }
            }catch (Exception e){
                e.printStackTrace();
                //  记录错误数据
                System.out.println("开始记录错误日志");
                NewsPO newsError = new NewsPO();
                String id = rowArray[0];  // id
                newsError.setId(id);
                newsError.setData_file_name(data_file_name);//  来源文件名
                newsError.setNews_basic_net_name(news_basic_net_name);// 网站名
                newsError.setIn_date(in_date);  //  录入时间
                newsError.setContent(String.valueOf(info));
                // 查询错误日志是否存在
                int count_flag = dao.getNewsLogCountSome(newsError);
                try{
                    if (count_flag > 0){  // 存在  就修改  更新日期
                        boolean update_flag = dao.updateNewsLogBySome(newsError);
                        if(update_flag != true){
                            System.out.println(" 更新错误日志报错，程序继续");
                            r_flag = false;
                        }
                    }else {  //  记录日志
                        boolean saveErrorLog =  dao.saveNewsLog(newsError);
                        if (saveErrorLog == false){
                            System.out.println("记录错误日志报错，程序继续");
                            r_flag = false;
                        }
                    }
                }catch (Exception es){
                    NewsPO newsErrorERROR = new NewsPO();
                    newsErrorERROR.setId(UUIDUtil.getUUID());
                    newsErrorERROR.setData_file_name(data_file_name);//  来源文件名
                    newsErrorERROR.setNews_basic_net_name(news_basic_net_name);// 网站名
                    newsErrorERROR.setIn_date(in_date);  //  录入时间
                    newsErrorERROR.setContent("记录报错日志时，程序报错，请查看改文件");
                    dao.saveNewsLog(newsErrorERROR);
                }

            }
        }
        return r_flag;
    }

    /**
     * 删除文件
     * @param
     * @return
     */
    public boolean delNewsFile(String path){

        boolean d_flag = fileService.delFile(new File(path));
        System.out.println("删除结果=="+d_flag+"删除文件路径=="+path);
        return d_flag;
    }

    @Override
    public List<NewsNetDict> getDict(NewsNetDict dict) {
        List<NewsNetDict> infos = dao.getDict(dict);
        return infos;
    }

    @Override
    public int getDictCount(NewsNetDict dict) {
        int count = dao.getDictCount(dict);
        return count;
    }

    @Override
    public boolean saveDict(NewsNetDict dict) {
        //获取id
        String id = UUIDUtil.getUUID();
        dict.setUpdate_time(TimeUtil.timess());
        dict.setId(id);
        return dao.saveDict(dict);
    }

    @Override
    public boolean updateById(NewsNetDict dict) {
        dict.setUpdate_time(TimeUtil.timess());
        return dao.updateById(dict);
    }

    @Override
    public boolean deleteById(NewsNetDict dict) {
        return dao.deleteById(dict);
    }
}
