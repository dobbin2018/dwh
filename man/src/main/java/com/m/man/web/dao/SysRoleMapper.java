package com.m.man.web.dao;

import com.m.man.web.model.SysRole;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface SysRoleMapper {

    @Results({
        @Result(id=true,column = "id",property = "id"),
            @Result(column = "name",property = "name")
    })
    @Select("select * from sys_role where id in (select roles_id from sys_user_roles where sys_user_id=#{userid})")
    public List<SysRole> getSysRolesByUserId(@Param("userid")Integer userid);
}
