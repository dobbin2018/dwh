package com.m.man.web.model;

public class FilePO {
    private String id;

    private String fileName;
    private String fileSize;
    private String fileMD5;
    private String updateTime;

    private String oldFile;
    private String newFile;

    private String analysed;
    private String fileType;
    private String toDb;


    public FilePO() {
    }

    public FilePO(String fileName, String fileSize, String fileMD5, String updateTime) {
        this.fileName = fileName;
        this.fileSize = fileSize;
        this.fileMD5 = fileMD5;
        this.updateTime = updateTime;
    }

    public FilePO(String fileName, String fileSize, String fileMD5, String updateTime, String oldFile, String newFile) {
        this.fileName = fileName;
        this.fileSize = fileSize;
        this.fileMD5 = fileMD5;
        this.updateTime = updateTime;
        this.oldFile = oldFile;
        this.newFile = newFile;
    }

    public FilePO(String fileName, String fileSize, String fileMD5, String updateTime, String oldFile, String newFile, String analysed) {
        this.fileName = fileName;
        this.fileSize = fileSize;
        this.fileMD5 = fileMD5;
        this.updateTime = updateTime;
        this.oldFile = oldFile;
        this.newFile = newFile;
        this.analysed = analysed;
    }

    public FilePO(String fileName, String fileSize, String fileMD5, String updateTime, String oldFile, String newFile, String analysed, String fileType) {
        this.fileName = fileName;
        this.fileSize = fileSize;
        this.fileMD5 = fileMD5;
        this.updateTime = updateTime;
        this.oldFile = oldFile;
        this.newFile = newFile;
        this.analysed = analysed;
        this.fileType = fileType;
    }

    public FilePO(String fileName, String fileSize, String fileMD5, String updateTime, String oldFile, String newFile, String analysed, String fileType, String toDb) {
        this.fileName = fileName;
        this.fileSize = fileSize;
        this.fileMD5 = fileMD5;
        this.updateTime = updateTime;
        this.oldFile = oldFile;
        this.newFile = newFile;
        this.analysed = analysed;
        this.fileType = fileType;
        this.toDb = toDb;
    }


    public FilePO(String id, String fileName, String fileSize, String fileMD5, String updateTime, String oldFile, String newFile, String analysed, String fileType, String toDb) {
        this.id = id;
        this.fileName = fileName;
        this.fileSize = fileSize;
        this.fileMD5 = fileMD5;
        this.updateTime = updateTime;
        this.oldFile = oldFile;
        this.newFile = newFile;
        this.analysed = analysed;
        this.fileType = fileType;
        this.toDb = toDb;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getToDb() {
        return toDb;
    }

    public void setToDb(String toDb) {
        this.toDb = toDb;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public String getAnalysed() {
        return analysed;
    }

    public void setAnalysed(String analysed) {
        this.analysed = analysed;
    }

    public String getOldFile() {
        return oldFile;
    }

    public void setOldFile(String oldFile) {
        this.oldFile = oldFile;
    }

    public String getNewFile() {
        return newFile;
    }

    public void setNewFile(String newFile) {
        this.newFile = newFile;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileSize() {
        return fileSize;
    }

    public void setFileSize(String fileSize) {
        this.fileSize = fileSize;
    }

    public String getFileMD5() {
        return fileMD5;
    }

    public void setFileMD5(String fileMD5) {
        this.fileMD5 = fileMD5;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }
}
