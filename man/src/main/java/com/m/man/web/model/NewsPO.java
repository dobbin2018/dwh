package com.m.man.web.model;

public class NewsPO  extends  ContentPO{
    private String id;  // 主键
    private String news_basic_net_name; // 网站名
    private String news_basic_net_url;  // 网址
    private String a_text; //标题
    private String a_url;  // 标题链接

    private String data_file_name ;  //  来源文件名
    private String update_time;  // 最后修改时间

    private String in_date;  //  录入时间
    private String  in_man;  //  录入人
    private  String update_man;  //  修改人
    private String  data_file_time;  // 文件时间

    @Override
    public String toString() {
        String r = "id=="+id+",,news_basic_net_name=="+news_basic_net_name+",,news_basic_net_url=="+news_basic_net_url
                +",,a_text=="+a_text+",,a_url=="+a_url+",,data_file_name=="+data_file_name
                +",,update_time=="+update_time
                +",,in_date=="+in_date
                +",,in_man=="+in_man
                +",,update_man=="+update_man
                +",,data_file_time=="+data_file_time;
        return r;
    }

    public NewsPO() {
    }

    public NewsPO(String id, String news_basic_net_name, String news_basic_net_url, String a_text, String a_url, String data_file_name, String update_time) {
        this.id = id;
        this.news_basic_net_name = news_basic_net_name;
        this.news_basic_net_url = news_basic_net_url;
        this.a_text = a_text;
        this.a_url = a_url;
        this.data_file_name = data_file_name;
        this.update_time = update_time;
    }

    public NewsPO(String id, String news_basic_net_name, String news_basic_net_url, String a_text, String a_url, String data_file_name, String update_time, String in_date, String in_man, String update_man, String data_file_time) {
        this.id = id;
        this.news_basic_net_name = news_basic_net_name;
        this.news_basic_net_url = news_basic_net_url;
        this.a_text = a_text;
        this.a_url = a_url;
        this.data_file_name = data_file_name;
        this.update_time = update_time;
        this.in_date = in_date;
        this.in_man = in_man;
        this.update_man = update_man;
        this.data_file_time = data_file_time;
    }

    public String getIn_date() {
        return in_date;
    }

    public void setIn_date(String in_date) {
        this.in_date = in_date;
    }

    public String getIn_man() {
        return in_man;
    }

    public void setIn_man(String in_man) {
        this.in_man = in_man;
    }

    public String getUpdate_man() {
        return update_man;
    }

    public void setUpdate_man(String update_man) {
        this.update_man = update_man;
    }

    public String getData_file_time() {
        return data_file_time;
    }

    public void setData_file_time(String data_file_time) {
        this.data_file_time = data_file_time;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNews_basic_net_name() {
        return news_basic_net_name;
    }

    public void setNews_basic_net_name(String news_basic_net_name) {
        this.news_basic_net_name = news_basic_net_name;
    }

    public String getNews_basic_net_url() {
        return news_basic_net_url;
    }

    public void setNews_basic_net_url(String news_basic_net_url) {
        this.news_basic_net_url = news_basic_net_url;
    }

    public String getA_text() {
        return a_text;
    }

    public void setA_text(String a_text) {
        this.a_text = a_text;
    }

    public String getA_url() {
        return a_url;
    }

    public void setA_url(String a_url) {
        this.a_url = a_url;
    }

    public String getData_file_name() {
        return data_file_name;
    }

    public void setData_file_name(String data_file_name) {
        this.data_file_name = data_file_name;
    }

    public String getUpdate_time() {
        return update_time;
    }

    public void setUpdate_time(String update_time) {
        this.update_time = update_time;
    }
}
