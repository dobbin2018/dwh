package com.m.man.web.controller;

import com.m.man.web.appPO.ResponseInfo;
import com.m.man.web.model.TicketTrainPO;
import com.m.man.web.service.TicketTrainService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/mycode/ticket")
public class TicketTrainController {

    @Autowired
    private TicketTrainService service;

    @RequestMapping("getAll")
    public List<TicketTrainPO> getAll(@RequestBody TicketTrainPO ticketTrainPO) {
        List<TicketTrainPO> info = service.getAll(ticketTrainPO);

        return info;
    }
    @RequestMapping("getCount")
    public int getCount(@RequestBody TicketTrainPO ticketTrainPO) {
        int count = service.getCount(ticketTrainPO);
//        int count = 100 ;
        return count;
    }
}
