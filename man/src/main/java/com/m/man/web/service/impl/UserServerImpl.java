package com.m.man.web.service.impl;

import com.m.man.web.dao.UserDao;
import com.m.man.web.model.UserPO;
import com.m.man.web.service.UserServer;
import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.core.GrantedAuthority;
//import org.springframework.security.core.authority.SimpleGrantedAuthority;
//import org.springframework.security.core.userdetails.User;
//import org.springframework.security.core.userdetails.UserDetails;
//import org.springframework.security.core.userdetails.UserDetailsService;
//import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserServerImpl implements UserServer {

    @Autowired
    private UserDao userDao;

    @Override
    public UserPO getUser(UserPO userPO){
        UserPO user =userDao.getUser(userPO) ;

        return user;
    }



}
