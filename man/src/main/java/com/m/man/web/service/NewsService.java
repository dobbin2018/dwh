package com.m.man.web.service;

import com.github.pagehelper.PageInfo;
import com.m.man.web.model.FilePO;
import com.m.man.web.model.NewsNetDict;
import com.m.man.web.model.NewsPO;
import org.springframework.stereotype.Service;

import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Map;


public interface NewsService {

    PageInfo<NewsPO> getAllNews(NewsPO news,Integer pageNum,Integer pageSize); //  使用分页工具 查询数据

    List<NewsPO> getNews(NewsPO news);
    int getCount(NewsPO news);

    Map<String,Object> saveNews2(String path) throws UnsupportedEncodingException, FileNotFoundException;// 保存  新闻
    boolean saveNews(FilePO filePO) throws UnsupportedEncodingException, FileNotFoundException;// 保存  新闻
    boolean delNewsFile(String path); //  删除新闻文件

    List<NewsNetDict> getDict(NewsNetDict dict); //  查找字典
    int getDictCount(NewsNetDict dict);  // 字典数据量

    boolean saveDict(NewsNetDict dict);  // 保存字典
    boolean updateById(NewsNetDict dict);// 修改字典
    boolean deleteById(NewsNetDict dict);// 删除字典
}
