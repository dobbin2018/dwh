package com.m.man.web.dao;

import com.m.man.web.model.NewsNetDict;
import com.m.man.web.model.PathPO;
import com.m.man.web.model.ScriptPO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface PathDao {

    int getDictCount(PathPO dict);  // 字典数据量
    List<PathPO> getAllDictByPage(PathPO dict); //  使用分页工具 查询数据
    List<PathPO> getAllDict(); //  不分页查询数据

    boolean saveDict(PathPO dict); // 保存字典
}
