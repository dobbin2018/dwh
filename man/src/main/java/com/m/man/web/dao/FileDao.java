package com.m.man.web.dao;

import com.m.man.web.model.FilePO;
import com.m.man.web.model.NewsPO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface FileDao {

    List<FilePO> getFile(FilePO file); //  使用分页工具 查询数据
    int getCount(FilePO file);            //  计算  数量

    FilePO  getFileById(String id);             // 根据id  查询是否存在
    int getCountById(String id);                 // 根据id  查询是否存在

    boolean saveFile(FilePO file);              // 保存  上传的文件信息

    boolean updateFileLogBySome(FilePO file);    // 根据条件 更改   文件信息

    boolean updateGetFileType(FilePO file);  // 文件分类
    boolean updateGetFileToDb(FilePO file);  // 文件分类  入库

}
