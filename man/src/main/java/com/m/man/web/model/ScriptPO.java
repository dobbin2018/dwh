package com.m.man.web.model;

import java.util.Date;

public class ScriptPO {
    private String        id;
    private String        script_id;
    private String        script_name;
    private String        script_path;
    private String        script_type;
    private int        script_avable;
    private Date       update_time;
    private String        update_user;


    public ScriptPO() {
    }

    public ScriptPO(String id, String script_id, String script_name, String script_path, String script_type, int script_avable, Date update_time, String update_user) {
        this.id = id;
        this.script_id = script_id;
        this.script_name = script_name;
        this.script_path = script_path;
        this.script_type = script_type;
        this.script_avable = script_avable;
        this.update_time = update_time;
        this.update_user = update_user;
    }

    public ScriptPO(String id, String script_id, String script_name, String script_path, int script_avable, Date update_time, String update_user) {
        this.id = id;
        this.script_id = script_id;
        this.script_name = script_name;
        this.script_path = script_path;
        this.script_avable = script_avable;
        this.update_time = update_time;
        this.update_user = update_user;
    }

    public String getScript_type() {
        return script_type;
    }

    public void setScript_type(String script_type) {
        this.script_type = script_type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getScript_id() {
        return script_id;
    }

    public void setScript_id(String script_id) {
        this.script_id = script_id;
    }

    public String getScript_name() {
        return script_name;
    }

    public void setScript_name(String script_name) {
        this.script_name = script_name;
    }

    public String getScript_path() {
        return script_path;
    }

    public void setScript_path(String script_path) {
        this.script_path = script_path;
    }

    public int getScript_avable() {
        return script_avable;
    }

    public void setScript_avable(int script_avable) {
        this.script_avable = script_avable;
    }

    public Date getUpdate_time() {
        return update_time;
    }

    public void setUpdate_time(Date update_time) {
        this.update_time = update_time;
    }

    public String getUpdate_user() {
        return update_user;
    }

    public void setUpdate_user(String update_user) {
        this.update_user = update_user;
    }
}
