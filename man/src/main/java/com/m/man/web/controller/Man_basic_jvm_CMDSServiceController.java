package com.m.man.web.controller;


import com.m.man.web.model.Man_basic_jvm_CMDSPO;
import com.m.man.web.service.Man_basic_jvm_CMDSService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("mycode/jvmCMDS")
public class Man_basic_jvm_CMDSServiceController {
    
    @Autowired
    private Man_basic_jvm_CMDSService service;


    @RequestMapping("/getAllCMD")
    public List<Man_basic_jvm_CMDSPO> getAllCMD() {
        return service.getAllCMD();
    }


    @RequestMapping("/saveCMD")
    public boolean saveCMD(@RequestBody Man_basic_jvm_CMDSPO cmd) {
        return service.saveCMD(cmd);
    }


    @RequestMapping("/updateById")
    public boolean updateById(@RequestBody Man_basic_jvm_CMDSPO cmd) {
        return service.updateById(cmd);
    }


    @RequestMapping("/deleteById")
    public boolean deleteById(@RequestBody Man_basic_jvm_CMDSPO cmd) {
        return service.deleteById(cmd);
    }
}
