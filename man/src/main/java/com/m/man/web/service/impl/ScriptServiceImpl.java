package com.m.man.web.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.m.man.web.dao.ScriptDao;
import com.m.man.web.model.ScriptPO;
import com.m.man.web.service.ScriptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public  class ScriptServiceImpl implements ScriptService {

    @Autowired
    private ScriptDao dao;
    @Override
    public int getDictCount(ScriptPO dict) {
        System.out.println("ffff");
        int k = dao.getDictCount(dict);
        System.out.println("kkk=="+k+"<<");
        return k;
    }

    @Override
    public PageInfo<ScriptPO> getAllScript(ScriptPO dict,Integer pageNum,Integer pageSize) {
        PageHelper.startPage(pageNum,pageSize);
        System.out.println("时间=="+dict.getUpdate_time());
        List<ScriptPO> list = dao.getAllScript(dict);
        PageInfo<ScriptPO> pageInfo = new PageInfo<>(list);
        return pageInfo;
    }
}
