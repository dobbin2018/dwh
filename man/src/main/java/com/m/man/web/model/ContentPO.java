package com.m.man.web.model;

public class ContentPO {
    private String content;

    public ContentPO() {
    }

    public ContentPO(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
