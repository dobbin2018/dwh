package com.m.man.web.dao;


import com.m.man.web.model.Man_basic_jvm_CMDSPO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface Man_basic_jvm_CMDSDao {

    List<Man_basic_jvm_CMDSPO> getAllCMD(); // 获取全部

    boolean saveCMD(Man_basic_jvm_CMDSPO cmd);   // 保存 jvm指令
    boolean updateById(Man_basic_jvm_CMDSPO cmd);// 修改 jvm指令
    boolean deleteById(Man_basic_jvm_CMDSPO cmd);// 删除 jvm指令

}
