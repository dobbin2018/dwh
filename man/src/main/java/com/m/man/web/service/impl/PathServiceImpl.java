package com.m.man.web.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.m.man.util.FileOptionUtil;
import com.m.man.util.TimeUtil;
import com.m.man.util.UUIDUtil;
import com.m.man.web.dao.PathDao;
import com.m.man.web.dao.ScriptDao;
import com.m.man.web.model.PathPO;
import com.m.man.web.service.PathService;
import com.m.man.web.service.ScriptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.nio.file.Path;
import java.util.List;

@Service
public  class PathServiceImpl implements PathService {

    @Autowired
    private PathDao dao;
    @Override
    public int getDictCount(PathPO dict) {
        System.out.println("ffff");
        int k = dao.getDictCount(dict);
        System.out.println("kkkfffff=="+k+"<<");
        return k;
    }

    @Override
    public PageInfo<PathPO> getAllDictByPage(PathPO dict,Integer pageNum,Integer pageSize) {
        PageHelper.startPage(pageNum,pageSize);
        System.out.println("时间=="+dict.getUpdate_time());
        List<PathPO> list = dao.getAllDictByPage(dict);
        PageInfo<PathPO> pageInfo = new PageInfo<>(list);
        return pageInfo;
    }
    @Override
    public List<PathPO> getAllDict() {
        List<PathPO> list = dao.getAllDict();
        return list;
    }

    @Override
    public boolean saveDict(PathPO dict) {
        //获取id
        String id = UUIDUtil.getUUID();
        dict.setUpdate_time(TimeUtil.timeDay());
        dict.setId(id);
        return dao.saveDict(dict);
    }

    /**
     *  检查    路径是否存在  不存在  则创建
     */
    @Override
    public boolean mkdirPath(String path){
        System.out.println("jjjjj=="+path);
        return FileOptionUtil.mkDir(path);
    }

}
