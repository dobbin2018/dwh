package com.m.man.web.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.m.man.web.model.ProductPO;


//@Repository
@Mapper
public interface IProductDao {
	 List<ProductPO> getAllProducts();

}
