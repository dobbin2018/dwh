package com.m.man.web.model;

public class NewsNetDict {
    private String id;
    private String net;
    private String url;
    private String flag;
    private String code;
    private String update_time;
    private String update_user;

    private String type;

    public NewsNetDict(String id, String net, String url, String flag, String code, String update_time, String update_user, String type) {
        this.id = id;
        this.net = net;
        this.url = url;
        this.flag = flag;
        this.code = code;
        this.update_time = update_time;
        this.update_user = update_user;
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNet() {
        return net;
    }

    public void setNet(String net) {
        this.net = net;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getUpdate_time() {
        return update_time;
    }

    public void setUpdate_time(String update_time) {
        this.update_time = update_time;
    }

    public String getUpdate_user() {
        return update_user;
    }

    public void setUpdate_user(String update_user) {
        this.update_user = update_user;
    }

    public NewsNetDict() {
    }

    public NewsNetDict(String id, String net, String url, String flag, String code, String update_time, String update_user) {
        this.id = id;
        this.net = net;
        this.url = url;
        this.flag = flag;
        this.code = code;
        this.update_time = update_time;
        this.update_user = update_user;
    }

}
