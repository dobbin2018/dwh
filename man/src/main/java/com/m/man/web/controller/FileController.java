package com.m.man.web.controller;

import com.m.man.util.FileOptionUtil;
import com.m.man.util.UUIDUtil;
import com.m.man.web.model.FilePO;
import com.m.man.web.service.FileService;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

@RequestMapping("/mycode/file")
@RestController
public class FileController {
     // 仓库路径
    @Value("${UPLOAD.file.home_path}")
    private String home_path ;

    @Autowired
    private FileService service;

    /**
     * 分类入库
     * @param file
     * @return
     */
    @RequestMapping("/toDb")
    public boolean updateGetFileToDb(@RequestBody FilePO file) {
        return service.updateGetFileToDb(file);
    }

    /**
     * 文件  分类
     * 根据文件的 后缀分类
     */
    @RequestMapping("/classFile")
    public boolean classFile(@RequestBody FilePO filePO){
        boolean r_flag = service.updateGetFileType(filePO);
        // 保存
        return r_flag;
    }

    /**
     * 查找  仓库中的文件信息
     */
    @RequestMapping("/getFile")
    public List<FilePO> getFile(@RequestBody Map<String,String> o){
        FilePO filePO = new FilePO();

        String fileMD5 = o.get("fileMD5");
        String fileName = o.get("fileName");
        String fileSize = o.get("fileSize");
        String updateTime = o.get("updateTime");
        String oldFile = o.get("oldFile");
        String newFile = o.get("newFile");
        String analysed = o.get("analysed");

        String toDb = o.get("toDb");

        filePO.setId(UUIDUtil.getUUID());
        filePO.setFileMD5(fileMD5);
        filePO.setFileName(fileName);
        filePO.setFileSize(fileSize);
        filePO.setUpdateTime(updateTime);
        filePO.setOldFile(oldFile);
        filePO.setNewFile(newFile);
        filePO.setAnalysed(analysed);
        filePO.setToDb(toDb);

        int pageSize = Integer.valueOf( o.get("pageSize"));
        int pageNum = Integer.valueOf(o.get("pageNum"));
        List<FilePO> list  = service.getFile(filePO,pageNum,pageSize) ;
        return list;
    }
    @RequestMapping("/getCount")
    public int getCount(@RequestBody FilePO filePO){
        int count = service.getCount(filePO);
        System.out.println("返回结果=="+count);
        return count;
    }

    /**
     * 文件 转移  将文件从仓库转移到   对应的文件  目录下
     */
    @RequestMapping("/moveFile")
    public Map<String,Object> moveFile(@RequestBody FilePO file) throws IOException {
        Map<String,Object> r_m = new HashMap<String, Object>();
        File oldFile = new File(file.getOldFile()) ;
        File newFile = new File(file.getNewFile());
        System.out.println("就路径=="+oldFile+"==========");
        System.out.println("新路径=="+newFile+"==========");
        long r = FileOptionUtil.copFileFileChannels(oldFile,newFile);
        if (r > 0){
            System.out.println("复制成功，结果===="+r);
            r_m.put("flag",true);
            r_m.put("path",oldFile);
        }else {
            System.out.println("复制失败");
            r_m.put("flag",false);
        }
        return r_m;
//        int r = FileOptionUtil.copyFile(oldFile,newFile);
//        if (r > 0 ){
//            System.out.println("复制成功，结果===="+r);
//            r_m.put("flag",true);
//            r_m.put("path",oldFile);
//            return  r_m;
//        }else{
//            System.out.println("复制失败");
//            r_m.put("flag",false);
//            return r_m;
//        }
    }

    /**
     * 删除  文件
     */
    @RequestMapping("/delFile")
    public boolean delFile(@RequestBody FilePO file){
        boolean r_flag = false;
        File oldFile = new File(file.getOldFile()) ;
            System.out.println("要删除的文件=="+oldFile+"<<");
            boolean delete_flag =service.delFile(oldFile);
            if(delete_flag == true){

                System.out.println("删除结果成功");
                return true;
            }else{
                System.out.println("删除结果失败");
                return false;
            }

    }

    /**
     * 获取指定 路径下的文件
     */
    @RequestMapping("/getFileByPath")
    public List<FilePO> getFileByPath(@RequestBody Map<String,String> o) throws IOException {
        String path = o.get("path");
        List<FilePO> list = service.getFile2(path) ;
        return list;
    }

}
