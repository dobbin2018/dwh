package com.m.man.web.service;

import com.m.man.web.model.Man_basic_jvm_CMDSPO;

import java.util.List;

public interface Man_basic_jvm_CMDSService {
    List<Man_basic_jvm_CMDSPO> getAllCMD(); // 获取全部

    boolean saveCMD(Man_basic_jvm_CMDSPO cmd);   // 保存 jvm指令
    boolean updateById(Man_basic_jvm_CMDSPO cmd);// 修改 jvm指令
    boolean deleteById(Man_basic_jvm_CMDSPO cmd);// 删除 jvm指令
}
