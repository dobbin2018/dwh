package com.m.man.web.model;

/**
 * dict_filename_info
 */
public class FileNamePO {
    private String id          ;
    private String filename    ;
    private String en_name     ;
    private String flag        ;
    private String update_user ;
    private String update_time ;
    private String avable      ;

    public FileNamePO(String id, String filename, String en_name, String flag, String update_user, String update_time, String avable) {
        this.id = id;
        this.filename = filename;
        this.en_name = en_name;
        this.flag = flag;
        this.update_user = update_user;
        this.update_time = update_time;
        this.avable = avable;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getEn_name() {
        return en_name;
    }

    public void setEn_name(String en_name) {
        this.en_name = en_name;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getUpdate_user() {
        return update_user;
    }

    public void setUpdate_user(String update_user) {
        this.update_user = update_user;
    }

    public String getUpdate_time() {
        return update_time;
    }

    public void setUpdate_time(String update_time) {
        this.update_time = update_time;
    }

    public String getAvable() {
        return avable;
    }

    public void setAvable(String avable) {
        this.avable = avable;
    }
}
