package com.m.man.web.service;

import com.m.man.web.model.UserPO;

public interface UserServer {

    UserPO getUser(UserPO userPO) ;
}
