package com.m.man.web.controller;

import com.m.man.queue.kafka.KafkaProducerInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/mycode/news")
@RestController
public class KafkaTestController {

    @Autowired
//    private KafkaTemplate<String, String> kafkaTemplate;
    private KafkaProducerInterface producer;

    @RequestMapping("/testSendMsg")
    public String testSendMsg() {
        String name =  "这是我发送的第一条信息";
        System.out.println("开始请求kafka");
        try {
            producer.send("textTopic", name);
        } catch (Exception e) {
            e.printStackTrace();
            return "false";
        }
        return "success";
    }
}
