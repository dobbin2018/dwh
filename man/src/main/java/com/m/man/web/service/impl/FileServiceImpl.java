package com.m.man.web.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.m.man.util.FileOptionUtil;
import com.m.man.web.dao.FileDao;
import com.m.man.web.model.FilePO;
import com.m.man.web.model.NewsPO;
import com.m.man.web.service.FileService;
import com.m.man.web.service.PathService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
public class FileServiceImpl implements FileService {

    @Autowired
    private FileDao dao;

    @Autowired
    private PathService pathService;

    /**
     *   查库 获取文件 信息
     */
    @Override
    public List<FilePO> getFile(FilePO filePO ,Integer pageNum,Integer pageSize){
        PageHelper.startPage(pageNum,pageSize);
        List<FilePO> list = dao.getFile(filePO);
        PageInfo<FilePO> pageInfo = new PageInfo<FilePO>(list);
        List<FilePO> r_list = pageInfo.getList();
        return r_list;
    }

    @Override
    public boolean updateGetFileType(FilePO filePO) {
        String oldFile = filePO.getOldFile();
        String typeName = FileOptionUtil.getFileType(oldFile);

        FilePO file = new FilePO();
        file.setFileMD5(filePO.getFileMD5());
        file.setAnalysed("true");
        file.setFileType(typeName);
        boolean r_flag = dao.updateGetFileType(file);
        return r_flag;
    }

    @Override
    public boolean updateGetFileToDb(FilePO file) {

        return dao.updateGetFileToDb(file);
    }

    /**
     *  查库  获取 数量
     */
    public int getCount(FilePO filePO){
        return dao.getCount(filePO);
    }

    /**
     *   保存  文件数据
     */
    @Override
    public boolean saveFile(FilePO filePO) {

        // 检查  文件是否 存在
        int count = dao.getCountById(filePO.getFileMD5());
        if (count == 0 ){  //  不存在  需要保存
            filePO.setAnalysed("false");
            filePO.setFileType("false");
            filePO.setToDb("false");
            boolean s_flag =  dao.saveFile(filePO);
            if (s_flag){
                return true;
            }else {
                return false;
            }
        }else{
            return true;
        }
    }

    /**
     *  根据文件 路径  获取文件信息
     */
    public FilePO getFileInfos(String path) throws IOException {
        FilePO file = new FilePO();
        // 获取绝对路径
        file.setOldFile(path);
        // 获取md5
        String md5 = new FileOptionUtil().getFileMD5(path);
        file.setFileMD5(md5);

//        // 获取文件名
        String name = new FileOptionUtil().getFileName(path);
        file.setFileName(name);
//        // 获取文件大小
        String size = String.valueOf(new FileOptionUtil().getFileSize(path));
        file.setFileSize(size);
//
//        // 获取 最后修改时间
        String time = new FileOptionUtil().getFileUpdateTimeSimple(path);
        file.setUpdateTime(time);
        return file;
    }


    /**
     * 获取  文件
     */
    @Override
    public List<FilePO> getFile2(String path) throws IOException {
        List<FilePO> lFile = new ArrayList<FilePO>();
        List<String> filePath =  new FileOptionUtil().getAllfileFullPath(path);
        for(String paths :filePath){
            FilePO file = new FilePO();
            // 获取绝对路径
            file.setOldFile(paths);
            // 获取文件名
            String name = new FileOptionUtil().getFileName(paths);
            file.setFileName(name);
            // 获取文件大小
            String size = String.valueOf(new FileOptionUtil().getFileSize(paths));
            file.setFileSize(size);
            // 获取md5
            String md5 = new FileOptionUtil().getFileMD5(paths);
            file.setFileMD5(md5);
            // 获取 最后修改时间
            String time = new FileOptionUtil().getFileUpdateTimeSimple(paths);
            file.setUpdateTime(time);
            lFile.add(file);
        }
        return lFile;
    }

    @Override
    public int getCount2(String path) {
        int count = new FileOptionUtil().getAllfileFullPath(path).size();
        return count;
    }

    @Override
    public boolean delFile(File path) {
        // 转移成功  删除文件
        boolean delete_flag = new FileOptionUtil().deleteFile(path);
        if(delete_flag == true){
            System.out.println("删除结果===成功");
            return true;
        }else{
            System.out.println("删除结果===失败");
            return false;
        }
    }
}
