package com.m.man.web.controller;

import com.m.man.util.FileOptionUtil;
import com.m.man.web.model.FilePO;
import com.m.man.web.model.UploadFilePO;
import com.m.man.web.service.FileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

//import javax.servlet.ServletException;
//import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.util.HashMap;
import java.util.Map;


@RequestMapping("mycode/upload")
@RestController
public class UpLoadController {
    @Value("${UPLOAD.file.home_path}")
    private String home_path ;

    @Autowired
    private FileService fileService;

    @RequestMapping("/uploadtxt")
    public Map getFile(@RequestParam("file") MultipartFile filed , @RequestParam String path) throws  IOException {
        Map map = new HashMap();
        String fileOldName = filed.getOriginalFilename();
        System.out.println(fileOldName);
        String name = filed.getName();
        String type = filed.getContentType();
        System.out.println(name +"  "+ type);
//
//        InputStream files = filed.getInputStream();
//        BufferedReader bre  = new BufferedReader(new InputStreamReader(files));
//        String str = "";
//        String resp = "";
//        while ((str = bre.readLine()) !=  null ){
//            resp = resp  + str+"\n";
//        }
//        files.close();
//        bre.close();

        //  写到指定路径 下
        if(filed.isEmpty()) {
            map.put("flag","请选择文件");
            return map;
        }
        String newFilePath = home_path+"\\primary"+"\\"+fileOldName;
        File file = new File(newFilePath);
        filed.transferTo(file);
        file.exists();

        map.put("flag","成功");

        //  获取文件  信息
        System.out.println("路径==="+newFilePath);
        FilePO filePO = fileService.getFileInfos(newFilePath);
        boolean s_flag = fileService.saveFile(filePO);
        if (s_flag == true){
            map.put("s_flag","报文文件成功");
            return map;
        }else{
            map.put("s_flag","报文文件成功");
            return map;
        }

        //读取  上传到路径下的文件
//        BufferedReader read=new BufferedReader(new InputStreamReader(new   FileInputStream(file),"GBK"));
//        BufferedReader read=new BufferedReader(new InputStreamReader(new   FileInputStream(file)));
    }
}
