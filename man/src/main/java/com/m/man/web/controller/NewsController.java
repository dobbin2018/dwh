package com.m.man.web.controller;

import com.github.pagehelper.PageInfo;
import com.m.man.ManApplication;
import com.m.man.web.model.FilePO;
import com.m.man.web.model.NewsNetDict;
import com.m.man.web.model.NewsPO;
import com.m.man.web.service.NewsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Map;


@RequestMapping("/mycode/news")
@RestController
public class NewsController {

    private static final Logger logger = LoggerFactory.getLogger(NewsController.class);

    @Autowired
    private NewsService service;

    /**
     * 使用 分页工具  分页查询
     * @return
     */
    @RequestMapping("/getAllNews")
    public List<NewsPO> getAllNews(@RequestBody Map<String,String> o) {
        logger.info("查询今日新闻");
        NewsPO news = new NewsPO(
                o.get("id"),o.get("news_basic_net_name"),o.get("news_basic_net_url"),
                o.get("a_text"),
                o.get("a_url"),
                o.get("String data_file_name"),
                o.get("update_time")
        );
        PageInfo<NewsPO>  pageInfo = service.getAllNews(news,Integer.valueOf(o.get("pageNum").toString()),Integer.valueOf(o.get("pageSize").toString()));
        List<NewsPO> list = pageInfo.getList();
        return  list;
    }

    @RequestMapping("/getCount")
    public int getPage(@RequestBody NewsPO news){
        int count = service.getCount(news);
        System.out.println("返回结果=="+count);
        return count;
    }

    @RequestMapping("/getNews")
    public List<NewsPO> getNews(@RequestBody NewsPO news){
        List<NewsPO> infos = service.getNews(news);
        System.out.println("返回信息=="+infos);
        return infos;
    }

    /**
     *   字典 条数
     */
    @RequestMapping("/getDictCount")
    public int getDictCount(@RequestBody NewsNetDict dict){
        int num = service.getDictCount(dict);
        return num;
    }
    /**
     *  查看  网站 字典
     */
    @RequestMapping("/getDict")
    public List<NewsNetDict>  getDict(@RequestBody NewsNetDict dict){
        return service.getDict(dict);
    }

    /**
     * 保存新闻字典
     */
    @RequestMapping("/save")
    public boolean saveNewDict(@RequestBody NewsNetDict dict){
        return service.saveDict(dict);
    }

    /**
     * 修改  新闻字典
     */
    @RequestMapping("/update")
    public boolean updateById(@RequestBody NewsNetDict dict){
        return service.updateById(dict);
    }

    /**
     * 删除  新闻字典
     */
    @RequestMapping("/deleteById")
    public boolean deleteById(@RequestBody NewsNetDict dict){
        return service.deleteById(dict);
    }

    /**
     * 文件结构化  入库
     */
    @RequestMapping("/saveNews")
    public boolean saveNews(@RequestBody FilePO file ) throws UnsupportedEncodingException, FileNotFoundException {
        String path = file.getOldFile();
        System.out.println("文件路径=="+path);
        boolean r_flag = service.saveNews(file);
        return r_flag;
    }

    /**
     *  删除  新闻数据文件
     */
    public boolean delNewsFile(@RequestBody FilePO file){
        String path = file.getOldFile();
        boolean r_flag = service.delNewsFile(path);
        return r_flag;
    }
}
