package com.m.man.web.controller;

//import org.springframework.security.core.Authentication;
import com.m.man.web.appPO.ResponseInfo;
import com.m.man.web.model.UserPO;
import com.m.man.web.service.UserServer;
import com.m.man.web.service.impl.SysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.Collection;
import java.util.Map;

@RequestMapping("/mycode/Sys")
@RestController
public class LoginController {

    @Autowired
    private UserServer userServer;


//    public String loginSuccess(HttpSession session , Authentication auth){
    @RequestMapping("/logins")
    public ResponseInfo loginSuccess(@RequestBody Map<String,String> m ){
        String name = m.get("username") ;
        String pwd  = m.get("password");

        ResponseInfo r = new ResponseInfo();

       UserPO user =  userServer.getUser(new UserPO(name,pwd));
//        UserPO user = new UserPO();
//        user.setId("1_3");
//        user.setUsername("lisi");
//        user.setPassword("1234");
        if ( null != user){
           System.out.println("找到数据=="+user.getUsername());
           r.setRESP_CODE(true);
           r.setError(null);
           r.setMessage(user);
           return r;
       }else{
           r.setError("没有找到数据！");
           return r;
       }

//        System.out.println("登录Controller");
//        Collection su = auth.getAuthorities();
//        StringBuffer roleStr = new StringBuffer();
//        for (Object sr : su) {
//            roleStr.append(sr);
//        }
//        String roles = su.toString();
//        String nameAndRole = "用户名:" + auth.getName() + "   角色:" + roles; // 主体名，即登录用户名
//        session.setAttribute("user_session",nameAndRole);
//        session.setAttribute("user_name",auth.getName());
//        session.setAttribute("user_roles",roleStr);

    }

    @RequestMapping("/logout")
    public String login(HttpSession session){
        session.invalidate();
        return "重新使用：login";
    }
}
