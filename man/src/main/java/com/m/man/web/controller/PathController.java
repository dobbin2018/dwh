package com.m.man.web.controller;

import com.github.pagehelper.PageInfo;
import com.m.man.util.TimeUtil;
import com.m.man.web.model.NewsNetDict;
import com.m.man.web.model.PathPO;
import com.m.man.web.model.ScriptPO;
import com.m.man.web.service.PathService;
import com.m.man.web.service.ScriptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RequestMapping("/mycode/path")
@RestController
public class PathController {

    @Autowired
    private PathService service;
    @RequestMapping("/getAllDict")
    public List<PathPO> getAllDict(@RequestBody Map<String,String> o) {

        List<PathPO> list  = service.getAllDict();
        return  list;
    }

    /**
     * 使用 分页工具  分页查询
     * @return
     */
    @RequestMapping("/getAllDictByPage")
    public List<PathPO> getAllDictByPage(@RequestBody Map<String,String> o) {
        PathPO path = new PathPO();
        path.setPath_name(o.get("path_name"));
        path.setPath_path(o.get("path_path"));
        path.setPath_type(o.get("path_type"));
        path.setUpdate_user(o.get("update_user"));
        path.setUpdate_time(o.get("update_time"));
        String path_avalbe_v =  o.get("path_avable") ;
        if("" != path_avalbe_v){
            path.setPath_avable(Integer.valueOf(path_avalbe_v));
        }
        PageInfo<PathPO> pageInfo = service.getAllDictByPage(path,
                Integer.valueOf(o.get("pageNum").toString()),
                Integer.valueOf(o.get("pageSize").toString()));
        List<PathPO> list = pageInfo.getList();
        return  list;
    }

    @RequestMapping("/getCount")
    public int getPage(@RequestBody PathPO path){
        PathPO vv = path;
        int count = service.getDictCount(path);
        System.out.println("返回结果=="+count);
        return count;
    }

    /**
     * 保存新闻字典
     */
    @RequestMapping("/save")
    public boolean saveNewDict(@RequestBody PathPO dict){
        return service.saveDict(dict);
    }

}
