package com.m.man.web.model;

import java.util.Date;

public class PathPO {
    private String        id;
    private String        path_id;
    private String        path_name;
    private String        path_path;
    private String        path_type;
    private int        path_avable;
    private String       update_time;
    private String        update_user;


    public PathPO() {
    }

    public PathPO(String id, String path_id, String path_name, String path_path, String path_type, int path_avable, String update_time, String update_user) {
        this.id = id;
        this.path_id = path_id;
        this.path_name = path_name;
        this.path_path = path_path;
        this.path_type = path_type;
        this.path_avable = path_avable;
        this.update_time = update_time;
        this.update_user = update_user;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPath_id() {
        return path_id;
    }

    public void setPath_id(String path_id) {
        this.path_id = path_id;
    }

    public String getPath_name() {
        return path_name;
    }

    public void setPath_name(String path_name) {
        this.path_name = path_name;
    }

    public String getPath_path() {
        return path_path;
    }

    public void setPath_path(String path_path) {
        this.path_path = path_path;
    }

    public String getPath_type() {
        return path_type;
    }

    public void setPath_type(String path_type) {
        this.path_type = path_type;
    }

    public int getPath_avable() {
        return path_avable;
    }

    public void setPath_avable(int path_avable) {
        this.path_avable = path_avable;
    }

    public String getUpdate_time() {
        return update_time;
    }

    public void setUpdate_time(String update_time) {
        this.update_time = update_time;
    }

    public String getUpdate_user() {
        return update_user;
    }

    public void setUpdate_user(String update_user) {
        this.update_user = update_user;
    }
}
