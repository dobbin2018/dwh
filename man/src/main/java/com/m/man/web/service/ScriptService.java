package com.m.man.web.service;

import com.github.pagehelper.PageInfo;
import com.m.man.web.model.ScriptPO;

public interface ScriptService {

    int getDictCount(ScriptPO dict);  // 字典数据量
    PageInfo<ScriptPO> getAllScript(ScriptPO dict, Integer pageNum, Integer pageSize); //  使用分页工具 查询数据
}
