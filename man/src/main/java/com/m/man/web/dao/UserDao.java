package com.m.man.web.dao;

import com.m.man.web.model.UserPO;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UserDao {
    UserPO getUser(UserPO userPO );//  获取 用户信息
}
