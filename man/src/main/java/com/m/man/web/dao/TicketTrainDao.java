package com.m.man.web.dao;

import com.m.man.web.model.TicketTrainPO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface TicketTrainDao {

    // 查询  火车票
    List<TicketTrainPO> getAll(TicketTrainPO ticketTrainPO);
    int getCount(TicketTrainPO ticketTrainPO);
}
