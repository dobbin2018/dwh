package com.m.man.web.model;

public class Man_basic_jvm_CMDSPO {

    private String code     ;
    private String agent    ;
    private String remark   ;

    public Man_basic_jvm_CMDSPO() {
    }

    public Man_basic_jvm_CMDSPO(String code, String agent, String remark) {
        this.code = code;
        this.agent = agent;
        this.remark = remark;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getAgent() {
        return agent;
    }

    public void setAgent(String agent) {
        this.agent = agent;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
