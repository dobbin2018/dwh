package com.m.man.web.service.impl;

import com.m.man.web.dao.TicketTrainDao;
import com.m.man.web.model.TicketTrainPO;
import com.m.man.web.service.TicketTrainService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TicketTrainServiceImpl implements TicketTrainService {

    @Autowired
    private TicketTrainDao dao;

    @Override
    public List<TicketTrainPO> getAll(TicketTrainPO ticketTrainPO) {
        List<TicketTrainPO> info = dao.getAll(ticketTrainPO);
        return info;
    }

    @Override
    public int getCount(TicketTrainPO ticketTrainPO) {
        int count = dao.getCount(ticketTrainPO);
        return count;
    }
}
