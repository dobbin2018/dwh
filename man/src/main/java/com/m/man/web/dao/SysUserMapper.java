package com.m.man.web.dao;

import com.m.man.web.model.SysUser;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.mapping.FetchType;

@Mapper
public interface SysUserMapper {
    @Insert("insert into sys_user (username,password) value(#{username},#{password})")
    int insert(SysUser user);

    @Results({
            @Result(id=true,column="id",property="id"),
            @Result(column="username",property="username"),
            @Result(column="password",property="password"),
            //@Result(column="email",property="email"),
            @Result(column="id",property="roles",
                    many=@Many(
                            select="com.m.man.web.dao.SysRoleMapper.getSysRolesByUserId",fetchType= FetchType.EAGER
                    )
            )
    })
    @Select("select * from sys_user where username=#{username} and password = #{password}")
    SysUser findSysUserByNameAndPwd(
            @Param("username") String username,
            @Param("password") String password
    );
}
