package com.m.man.web.service.impl;

import com.m.man.web.dao.Man_basic_jvm_CMDSDao;
import com.m.man.web.model.Man_basic_jvm_CMDSPO;
import com.m.man.web.service.Man_basic_jvm_CMDSService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class Man_basic_jvm_CMDSServiceImpl implements Man_basic_jvm_CMDSService {

    @Autowired
    private Man_basic_jvm_CMDSDao dao;

    @Override
    public List<Man_basic_jvm_CMDSPO> getAllCMD() {
        return dao.getAllCMD();
    }

    @Override
    public boolean saveCMD(Man_basic_jvm_CMDSPO cmd) {
        return dao.saveCMD(cmd);
    }

    @Override
    public boolean updateById(Man_basic_jvm_CMDSPO cmd) {
        return dao.updateById(cmd);
    }

    @Override
    public boolean deleteById(Man_basic_jvm_CMDSPO cmd) {
        return dao.deleteById(cmd);
    }
}
