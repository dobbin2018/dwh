package com.m.man.web.service;

import org.springframework.web.socket.server.standard.ServerEndpointExporter;

public interface WebSocketServer {
    public ServerEndpointExporter serverEndpointExporter();
}
