package com.m.man.web.service;

import com.github.pagehelper.PageInfo;
import com.m.man.web.model.PathPO;
import com.m.man.web.model.ScriptPO;

import java.util.List;

public interface PathService {

    int getDictCount(PathPO dict);  // 字典数据量
    PageInfo<PathPO> getAllDictByPage(PathPO dict, Integer pageNum, Integer pageSize); //  使用分页工具 查询数据
    List<PathPO> getAllDict(); //  不分页查询数据

    boolean saveDict(PathPO dict); // 保存字典

    boolean mkdirPath(String path);// 检查路径是否存在
}
