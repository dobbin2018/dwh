package com.m.man.web.dao;

import com.m.man.web.model.NewsNetDict;
import com.m.man.web.model.NewsPO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface NewsDao {
    List<NewsPO> getAllNews(NewsPO news); //  使用分页工具 查询数据

    List<NewsPO> getNews(NewsPO news);
    int getCount(NewsPO news);

    int getCountById(String id); // 根据id  查询是否存在

    boolean saveNews(NewsPO news);// 保存  新闻
    boolean saveNewsLog(NewsPO news);// 保存  新闻日志
    boolean updateNewsLogBySome(NewsPO news); // 根据条件 更改   错误日志
    int getNewsLogCountSome(NewsPO news);//根据条件查询   错误日志


    List<NewsNetDict> getDict(NewsNetDict dict); //  查找字典
    int getDictCount(NewsNetDict dict);  // 字典数据量

    boolean saveDict(NewsNetDict dict);  // 保存字典
    boolean updateById(NewsNetDict dict);// 修改字典
    boolean deleteById(NewsNetDict dict);// 删除字典

}
