package com.m.man.web.controller;

import com.github.pagehelper.PageInfo;
import com.m.man.util.TimeUtil;
import com.m.man.web.model.ScriptPO;
import com.m.man.web.service.ScriptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;
import java.util.Map;

@RequestMapping("/mycode/script")
@RestController
public class ScriptController {

    @Autowired
    private ScriptService service;

    /**
     * 使用 分页工具  分页查询
     * @return
     */
    @RequestMapping("/getAllScript")
    public List<ScriptPO> getAllNews(@RequestBody Map<String,String> o) {
        ScriptPO scritp = new ScriptPO();

        scritp.setScript_name(o.get("script_name"));
        scritp.setScript_path(o.get("script_path"));
        scritp.setScript_type(o.get("script_type"));
        scritp.setUpdate_user(o.get("update_user"));

        String script_avalbe_v =  o.get("script_avable") ;
        String update_time_v =  o.get("update_time") ;
        if("" != script_avalbe_v){
            scritp.setScript_avable(Integer.valueOf(script_avalbe_v));
        }
        if("" != update_time_v){
            scritp.setUpdate_time(TimeUtil.string2Date(update_time_v,"yyyy-MM-dd HH:mm:ss"));
        }
        PageInfo<ScriptPO> pageInfo = service.getAllScript(scritp,
                Integer.valueOf(o.get("pageNum").toString()),
//                Integer.valueOf("1"),
                Integer.valueOf(o.get("pageSize").toString()));
//                Integer.valueOf("10"));
        List<ScriptPO> list = pageInfo.getList();
        return  list;
    }

    @RequestMapping("/getCount")
    public int getPage(@RequestBody ScriptPO script){
        ScriptPO vv = script;
        int count = service.getDictCount(script);
        System.out.println("返回结果=="+count);
        return count;
    }
}
