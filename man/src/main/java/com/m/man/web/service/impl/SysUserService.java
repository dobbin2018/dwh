package com.m.man.web.service.impl;

import com.m.man.web.dao.SysUserMapper;
import com.m.man.web.model.SysUser;
import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.core.userdetails.UserDetails;
//import org.springframework.security.core.userdetails.UserDetailsService;
//import org.springframework.security.core.userdetails.UsernameNotFoundException;
//import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
//import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class SysUserService {


    @Autowired
    SysUserMapper sysUserMapper;


    public SysUser findUserByUsernameAndPwd(String username,String password) {
        SysUser user = sysUserMapper.findSysUserByNameAndPwd(username , password);
        if ( null == user ){
            System.out.println("用户不存在");
            return null;
        }else{
            System.out.println("找到用户了");
        }
        return user;
    }
}
