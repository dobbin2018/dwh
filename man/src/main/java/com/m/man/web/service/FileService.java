package com.m.man.web.service;

import com.github.pagehelper.PageInfo;
import com.m.man.web.model.FilePO;
import com.m.man.web.model.NewsPO;

import java.io.File;
import java.io.IOException;
import java.util.List;

public interface FileService {

    List<FilePO> getFile(FilePO file,Integer pageNum,Integer pageSize); //  使用分页工具 查询数据
    int getCount(FilePO file);            //  计算  数量

    boolean saveFile(FilePO filePO);  // 保存文件 信息到数据库
    FilePO getFileInfos(String path) throws IOException; //  获取 文件信息

    boolean updateGetFileType(FilePO file);  // 文件  分类

    boolean updateGetFileToDb(FilePO file);  // 文件分类  入库

    List<FilePO> getFile2(String path) throws IOException; // 检查目录下面的  文件

    int getCount2(String path);

    /**
     * 删除文件
     */
    boolean delFile(File path);
}
