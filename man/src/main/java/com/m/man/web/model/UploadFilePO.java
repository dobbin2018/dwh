package com.m.man.web.model;

import org.springframework.web.multipart.MultipartFile;

import java.io.Serializable;

public class UploadFilePO implements Serializable {
    private String id;
    private String path;
    private MultipartFile multipartFile;

    public UploadFilePO() {
    }

    public UploadFilePO(String id, String path, MultipartFile multipartFile) {
        this.id = id;
        this.path = path;
        this.multipartFile = multipartFile;
    }

    private static final long serialVersionUID = 1L;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public MultipartFile getMultipartFile() {
        return multipartFile;
    }

    public void setMultipartFile(MultipartFile multipartFile) {
        this.multipartFile = multipartFile;
    }
}
