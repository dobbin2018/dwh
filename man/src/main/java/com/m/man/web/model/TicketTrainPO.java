package com.m.man.web.model;

public class TicketTrainPO extends Page {
    private String  id             ;
    private String  date           ;
    private String  time_begin     ;
    private String  station_begin  ;
    private String  station_end    ;
    private String  cheCi          ;
    private String  pici           ;
    private String  update_time     ;
    private String  uuid           ;
    private String  status         ;

    public TicketTrainPO() {
    }

    public TicketTrainPO(String id, String date, String time_begin, String station_begin, String station_end, String cheCi, String pici, String update_time, String uuid, String status) {
        this.id = id;
        this.date = date;
        this.time_begin = time_begin;
        this.station_begin = station_begin;
        this.station_end = station_end;
        this.cheCi = cheCi;
        this.pici = pici;
        this.update_time = update_time;
        this.uuid = uuid;
        this.status = status;
    }

    public TicketTrainPO(int startRow, int showRow, String id, String date, String time_begin, String station_begin, String station_end, String cheCi, String pici, String update_time, String uuid, String status) {
        super(startRow, showRow);
        this.id = id;
        this.date = date;
        this.time_begin = time_begin;
        this.station_begin = station_begin;
        this.station_end = station_end;
        this.cheCi = cheCi;
        this.pici = pici;
        this.update_time = update_time;
        this.uuid = uuid;
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime_begin() {
        return time_begin;
    }

    public void setTime_begin(String time_begin) {
        this.time_begin = time_begin;
    }

    public String getStation_begin() {
        return station_begin;
    }

    public void setStation_begin(String station_begin) {
        this.station_begin = station_begin;
    }

    public String getStation_end() {
        return station_end;
    }

    public void setStation_end(String station_end) {
        this.station_end = station_end;
    }

    public String getCheCi() {
        return cheCi;
    }

    public void setCheCi(String cheCi) {
        this.cheCi = cheCi;
    }

    public String getPici() {
        return pici;
    }

    public void setPici(String pici) {
        this.pici = pici;
    }

    public String getUpdate_time() {
        return update_time;
    }

    public void setUpdate_time(String update_time) {
        this.update_time = update_time;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
