package com.m.man.web.dao;

import com.m.man.web.model.ScriptPO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface ScriptDao {

    int getDictCount(ScriptPO dict);  // 字典数据量
    List<ScriptPO> getAllScript(ScriptPO dict); //  使用分页工具 查询数据
}
