package com.m.man.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSON;
import com.m.man.web.model.ProductPO;
import com.m.man.web.service.ProductService;

@RestController
public class ProductController {
	
	 @Autowired
	   private ProductService productService;
	   
	   @RequestMapping("/products")
	   public String getAllProducts(){
	      List<ProductPO> allProducts = productService.getAllProducts();
	      System.out.println(allProducts.size());
	      return JSON.toJSONString(allProducts);
	   }

}
