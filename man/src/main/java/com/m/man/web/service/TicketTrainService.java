package com.m.man.web.service;

import com.m.man.web.model.TicketTrainPO;
import org.springframework.stereotype.Service;

import java.util.List;


public interface TicketTrainService {
    // 查询  火车票
    List<TicketTrainPO> getAll(TicketTrainPO ticketTrainPO);
    int getCount(TicketTrainPO ticketTrainPO);
}
