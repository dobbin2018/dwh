package com.m.man.web.vieWinfo;

import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * 全局 捕获异常
 */
//@RestControllerAdvice(basePackages = "com.m.man.web.controller")
public class GlobalExceptionHandler {

    //
    @ExceptionHandler(RuntimeException.class)
    @ResponseBody
    public Map errorHandler(RuntimeException ex){
//        System.out.println("全局捕获异常执行");
        Map map = new HashMap();
        map.put("code",100);
        map.put("msg",ex.getMessage());
        return map;
    }
}
