package com.m.man.web.controller;

import com.m.man.web.model.TicketTrainPO;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/mycode/test")
public class TestController {

    @RequestMapping("/getAll")
    public String getAll() {
        String info = "{'resp':'已经请求到了!'}";
//        try {
//            Thread.sleep(30000);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
        return info;
    }
}
