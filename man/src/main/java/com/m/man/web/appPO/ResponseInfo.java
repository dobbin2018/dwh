package com.m.man.web.appPO;


/**
 * 作为 响应类
 */
public class ResponseInfo {

    private Boolean RESP_CODE = false ;  //  响应  代码   true  or false
    private Object message ;   //  响应信息
    private String error   ;   // 报错信息

    public ResponseInfo() {
    }

    public ResponseInfo(Boolean RESP_CODE, Object message, String error) {
        this.RESP_CODE = RESP_CODE;
        this.message = message;
        this.error = error;
    }

    public Boolean getRESP_CODE() {
        return RESP_CODE;
    }

    public void setRESP_CODE(Boolean RESP_CODE) {
        this.RESP_CODE = RESP_CODE;
    }

    public Object getMessage() {
        return message;
    }

    public void setMessage(Object message) {
        this.message = message;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
