package com.m.man.logger;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.Arrays;

@Aspect
@Component
public class LogAspect {
    private final static Logger logger = LoggerFactory.getLogger(LogAspect.class);

    /**
     * 以 controller 包下定义的所有请求为切入点
     * ("execution(public * site.exception.springbootaopwebrequest.controller..*.*(..))")
     * 定义请求日志切入点，其切入点表达式有多种匹配方式,这里是指定路径
     * */
    @Pointcut("execution(public * com.m.man.web.controller.*.*(..))")
    public void log() {}

    /**
     * 在切点之前织入
     * @param joinPoint
     * @throws Throwable
     *  * 前置通知：
     *  * 1. 在执行目标方法之前执行，比如请求接口之前的登录验证;
     *  * 2. 在前置通知中设置请求日志信息，如开始时间，请求参数，注解内容等
     *  *
     */
    @Before("log()")
    public void doBefore(JoinPoint joinPoint) throws Throwable {
        // 接收到请求，记录请求内容
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attributes.getRequest();
        // 记录下请求内容
        logger.info("<<<<<<<<<<<<<<<<<<<<<<<<");
        logger.info("请求来源 : " + request.getRequestURL().toString());
        logger.info("请求方式 : " + request.getMethod());
        logger.info("IP地址 : " + request.getRemoteAddr());
        logger.info("请求接口 : " + joinPoint.getSignature().getDeclaringTypeName() + "." + joinPoint.getSignature().getName());
        logger.info("请求参数 : " + Arrays.toString(joinPoint.getArgs()));

        // 系统信息
//        logger.info("浏览器：{}", request.getBrowser().toString());
//        logger.info("浏览器版本：{}", request.getBrowserVersion());
//        logger.info("操作系统: {}", request.getOperatingSystem().toString());

    }

    /**
     * 在切点之后织入
     * @throws Throwable
     * 返回通知：
     *  * 1. 在目标方法正常结束之后执行
     *  * 1. 在返回通知中补充请求日志信息，如返回时间，方法耗时，返回值，并且保存日志信息
     *  *
     */
    @After("log()")
    public void doAfter() throws Throwable {
        logger.info("=========================================== End ===========================================");
        // 每个请求之间空一行
        logger.info("");
    }

    /**
     * 环绕
     * @param proceedingJoinPoint
     * @return
     * @throws Throwable
     * 在执行方法前后调用Advice，这是最常用的方法，相当于@Before和@AfterReturning全部做的事儿
     * * @param pjp
     */
    @Around("log()")
    public Object doAround(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        long startTime = System.currentTimeMillis();
        Object result = proceedingJoinPoint.proceed();
        // 打印出参
        logger.info("Response Args  : {}", result);
        // 执行耗时
        logger.info("Time-Consuming : {} ms", System.currentTimeMillis() - startTime);
        return result;
    }

    /**
     * 异常通知：
     * 1. 在目标方法非正常结束，发生异常或者抛出异常时执行
     * 1. 在异常通知中设置异常信息，并将其保存
     *
     * @param throwable
     */
    @AfterThrowing(value = "log()", throwing = "throwable")
    public void doAfterThrowing(Throwable throwable) {
        // 保存异常日志记录
        logger.error("发生异常时间：{}" + LocalDateTime.now());
        logger.error("抛出异常：{}" + throwable.getMessage());
    }

}
