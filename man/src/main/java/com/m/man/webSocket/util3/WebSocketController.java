package com.m.man.webSocket.util3;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

@Controller
@EnableScheduling   //定时任务支持
public class WebSocketController {

    @Autowired
//    private SimpMessagingTemplate messagingTemplate;

    @RequestMapping("/")
    public String index(){
        return "index";
    }

    /**
     * 这里用于客户端推送消息到服务端，推送地址为：setApplicationDestinationPrefixes("/app")设置得前缀加上@MessageMapping注解得地址
     * 此处为/app/send
     * 当前方法处理后将结果转发给@SendTo注解得地址，如果没有指定，则采用默认
     * @MessageMapping 指定要接收消息的地址，类似@RequestMapping。除了注解到方法上，也可以注解到类上
     * @SendTo 默认消息将被发送到与传入消息相同的目的地，但是目的地前面附加前缀（默认情况下为"/topic")
     * @param message 客户端推送过来得消息，一般生产环境会封装
     * @return
     * @throws Exception
     */
     @MessageMapping("/send")
//     @SendTo("/topicTest/web-to-server-to-web")
     public String greeting(String message) throws Exception {
          // 模拟延时，以便测试客户端是否在异步工作
          Thread.sleep(1000);
          return "Hello, " + message + "!";
      }

      /**
       * 最基本的服务器端主动推送消息给客户端
       * @return
       * @throws Exception
       */
     @Scheduled(initialDelay = 7000,fixedRate = 2000)
     public String serverTime() throws Exception {
         // 发现消息
         DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//         messagingTemplate.convertAndSend("/topicTest/servertime", df.format(new Date()));
         return "servertime";
     }
}
