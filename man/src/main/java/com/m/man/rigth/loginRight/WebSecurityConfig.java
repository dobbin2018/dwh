//package com.m.man.rigth.loginRight;
//
//import com.m.man.web.service.impl.SysUserService;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
//import org.springframework.security.config.annotation.web.builders.HttpSecurity;
//import org.springframework.security.config.annotation.web.builders.WebSecurity;
//import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
//import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
//import org.springframework.security.core.userdetails.UserDetailsService;
//import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
//import org.springframework.security.crypto.password.PasswordEncoder;
//
//@Configuration
//@EnableWebSecurity
//public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
//
//    @Bean
//    public PasswordEncoder passwordEncoder(){
//
//        return new BCryptPasswordEncoder();
//    }
//
//    @Bean
//    UserDetailsService detailsService(){
//
//        return new SysUserService();
//    }
//
//    @Override
//    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//        auth.userDetailsService(detailsService())   //用户认证
//             .passwordEncoder(passwordEncoder());//密码认证
//    }
//
//    @Override
//    public void configure(WebSecurity web) throws Exception {
////        web.ignoring().antMatchers("/config/**", "/css/**", "/fonts/**", "/images/**", "/js/**");
//    }
//
//    @Override
//    protected void configure(HttpSecurity http) throws Exception {
//        http
//                .authorizeRequests()
//                .antMatchers("/app/**").permitAll() // 注册请求不需要验证
//                .antMatchers("/master/**").permitAll() // 注册请求不需要验证
//                .antMatchers("/server/**").permitAll() // 注册请求不需要验证
//                .antMatchers("/vendor/**").permitAll() // 注册请求不需要验证
//                .antMatchers("/index.html").permitAll()
//                .antMatchers("/login.html").permitAll()
//                .antMatchers("/login").permitAll()
//                .anyRequest().authenticated()        //其他路径都需要认证
//
//                .and()
//                .formLogin()
//                .loginPage("/login")  //登录页面，加载登录的html页面
//                .loginProcessingUrl("/loginVlidate")  //发送Ajax请求的路径
//                .usernameParameter("username")//请求验证参数
//                .passwordParameter("password")//请求验证参数
////                .failureHandler(userLoginAuthenticationFailureHandler)//验证失败处理
////                .successHandler(userLoginAuthenticationSuccessHandler)//验证成功处理
//
////                .permitAll();//登录页面无需设置验证
//
//                .defaultSuccessUrl("/index", true)
//                .failureUrl("/login?error").permitAll()
//                .and()
//                .logout().logoutSuccessUrl("/login").permitAll()
////                .and().rememberMe()
//                .and()
//                .csrf().disable();// 跨站请求伪造,该令牌用于验证授权用户和发起请求者是否是同一个人
////                .and()    //完成上一个配置，进行下一步配置
////                .formLogin()  //配置表单登录
////                .loginPage("/login")   //设置登录页面
////                .loginProcessingUrl("/login")
////                .defaultSuccessUrl("/index", true)  // 设置成功处理器 *
////                .failureUrl("/login?error").permitAll()           // 设置失败处理器*/
////                .and().logout().logoutSuccessUrl("/login").permitAll()
////                .and().csrf().disable();// 跨站请求伪造,该令牌用于验证授权用户和发起请求者是否是同一个人
//    }
//}
