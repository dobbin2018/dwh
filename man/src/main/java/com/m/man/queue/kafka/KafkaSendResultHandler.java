package com.m.man.queue.kafka;



import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.support.ProducerListener;
import org.springframework.stereotype.Component;

/**
 * 消息结果回调类
 */
@Component
public class KafkaSendResultHandler implements ProducerListener {

    private static final Logger log = LoggerFactory.getLogger(KafkaSendResultHandler.class);

    /**
     *  消息  发送  成功
     * @param producerRecord
     * @param recordMetadata
     */
    public void onSuccess(ProducerRecord producerRecord, RecordMetadata recordMetadata) {
        log.info("信息 发送 成功 : " + producerRecord.toString());
    }

    /**
     * 信息 发送 失败
     * @param producerRecord
     * @param exception
     */
    public void onError(ProducerRecord producerRecord, Exception exception) {
        log.info("信息 发送 失败 : " + producerRecord.toString());
    }
}
