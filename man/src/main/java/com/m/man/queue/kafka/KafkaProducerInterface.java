package com.m.man.queue.kafka;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

public interface KafkaProducerInterface {
    public void send(String topic, String msg) throws ExecutionException, InterruptedException, TimeoutException;
}
