package com.m.man.util;

public class DateUtil {

	private int Year;
	private int Month;
	private int Date;
	private boolean isGregorianLeap;
	private int dayOfYear;
	private int dayOfWeek;
	private int chineseYear;
	private int chineseMonth;
	private int chineseDate;
	private int sectionalTer;
	private int principleTerm;
	
	//每月阳历多少天
	private static char[] daysWithMonth = {31,28,31,30,31,30,31,31,30,31,30,31};
	private static String[] stemNames = {"甲","乙","丙","丁","戊","己","庚","辛","壬","癸"};
	private static String[] branchNames = { "子", "丑", "寅", "卯", "辰", "巳", "午","未", "申", "酉", "戌", "亥" };
	private static String[] animalNames = { "鼠", "牛", "虎", "兔", "龙", "蛇", "马","羊", "猴", "鸡", "狗", "猪" };
}
