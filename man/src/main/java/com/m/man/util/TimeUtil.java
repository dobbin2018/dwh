package com.m.man.util;

import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class TimeUtil {
	/**
	 * 获取当前时间戳（13位）
	 * @return
	 */
	public static Long getTimeStamp(){
		return System.currentTimeMillis();
	}
	
	/**
	 * 日期  字符串  转成  日期
	 */
	public static Date string2Date(String time ,String type){
		SimpleDateFormat format = new SimpleDateFormat(type);
		Date date = null;
	        try {
				date = format.parse(time);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    return date;
	}
	
	/**
	 * 日期  日期转字符串
	 */
	public static String date2String(Date time,String type){
		SimpleDateFormat format = new SimpleDateFormat(type);
		String str = format.format(time);
		return str;
	}
	
	/**
	 * 获取  某个时间的  格式化时间
	 */
	public static String getFormartTime(long time){
		Calendar cal = Calendar.getInstance();  
		SimpleDateFormat  df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return df.format(cal.getTime());
	}
	
	/**
	 * 获取当前日期   年月日时分秒毫秒
	 * 格式：yyyy-MM-dd HH:mm:ss SSS
	 * @param
	 */
	public static String timeSSS(){
		SimpleDateFormat  df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss SSS");
		return df.format(new Date());
	}
	/**
	 * 获取当前日期   年月日时分秒
	 * 格式：yyyy-MM-dd HH:mm:ss
	 * @param
	 */
	public static String timess(){
		SimpleDateFormat  df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return df.format(new Date());
	}
	
	/**
	 * 获取当前日期   年月日时分
	 * 格式：yyyy-MM-dd HH:mm
	 * @param
	 */
	public static String timeMine(){
		SimpleDateFormat  df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		return df.format(new Date());
	}
	
	/**
	 * 获取当前日期   年月日
	 * 格式：yyyy-MM-dd
	 * @param
	 */
	public static String timeDay(){
		SimpleDateFormat  df = new SimpleDateFormat("yyyy-MM-dd");
		return df.format(new Date());
	}
	public static String timeDay(String format){
		SimpleDateFormat  df = new SimpleDateFormat(format);
		return df.format(new Date());
	}

	/**
	 * 将短时间格式时间转换为字符串 yyyy-MM-dd
	 *
	 * @param dateDate
	 * @return
	 */
	public static String dateToStr(java.util.Date dateDate) {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		String dateString = formatter.format(dateDate);
		return dateString;
	}

	/**
	 * 将短时间格式字符串转换为时间 yyyy-MM-dd
	 * @param strDate
	 * @return
	 */
	public static Date strToDate(String strDate) {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		ParsePosition pos = new ParsePosition(0);
		Date strtodate = formatter.parse(strDate, pos);
		return strtodate;
	}

	/**
	 * 从一种个是 转成 另一种格式
	 * @param time
	 * @param type1
	 * @param type2
	 * @return
	 */
	public static String timeType2Type(String time ,String type1, String type2){
		String rtime = null;
		try {
			Date date = new SimpleDateFormat(type1).parse(time);
			rtime = new SimpleDateFormat(type2).format(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return rtime;
	}


	public static void main(String[] args) {


		Long time = getTimeStamp();
		System.out.println(time);
		String t_sss = timeSSS();
		System.out.println(t_sss);
		Date kk = strToDate(t_sss);

		Date ll = string2Date(t_sss, "yyyy-MM-dd");

		System.out.println("======="+kk);
		System.out.println("ll======="+ll);
		String ss = timeType2Type(t_sss, "yyyy-MM-dd HH:mm:ss SSS", "yyyy-MM-dd");
		System.out.println("dddd==="+ss);
		System.out.println(timess());
		System.out.println(timeMine());
		System.out.println(timeDay());
		
	}

}
