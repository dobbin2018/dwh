package com.stream.utils

import java.text.SimpleDateFormat
import java.util.Date

object TimeUtil {

  /**
    * 时间格式 转化
    * @param time
    * @param type1
    * @param type2
    * @return
    */
  def timeType2Type(time:String,type1:String,type2:String): String ={
    var rtime = ""
    try{
      val date = new SimpleDateFormat(type1).parse(time)
      rtime = new SimpleDateFormat(type2).format(date)
    }catch {
      case ex:Exception => ex.printStackTrace()
    }
    rtime
  }

  /**
    * 获取当前日期   年月日时分秒毫秒
    * 格式：yyyy-MM-dd HH:mm:ss SSS
    * @param
    */
  def timeSSS: String = {
    val df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss SSS")
    df.format(new Date)
  }

  def main(args: Array[String]): Unit = {
    println("ffffff")
    val t = "2020-08-16 21:23:21 001"
    val kk = timeType2Type(t,"yyyy-MM-dd HH:mm:ss SSS","yyyy-MM-dd HH:mm:ss")
    println(kk)
  }
}
