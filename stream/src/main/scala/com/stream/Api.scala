package com.stream

import org.apache.flink.api.scala.ExecutionEnvironment
import org.apache.flink.api.scala._

/**
  * aggregation
  */
object Api {
  def main(args: Array[String]): Unit = {
    val senv = ExecutionEnvironment.getExecutionEnvironment
    val tupleStream : DataSet[(Int, Int, Int)] = senv.fromElements(
      (0, 0, 0), (0, 1, 1), (0, 2, 2),
      (1, 0, 6), (1, 1, 7), (1, 2, 8)
    )
    val keyedStream = tupleStream

    keyedStream.print()
  }
}
